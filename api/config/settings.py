"""Settings, or dynamic definitions."""
from .environment import DATABASE_HOST, DATABASE_PWD, DATABASE_USER, DATABASE_NAME


if DATABASE_HOST is None:
    database_uri = f"postgresql://localhost/{DATABASE_NAME}"
else:
    database_uri = "postgresql://{user}:{pwd}@{host}/{name}".format(
        user=DATABASE_USER,
        pwd=DATABASE_PWD,
        host=DATABASE_HOST,
        name=DATABASE_NAME
    )
