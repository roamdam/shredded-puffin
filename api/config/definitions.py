class HTTPMessages:
    """HTTP responses constants."""
    OK = "OK"
    ERROR = "Internal error"
    BAD_REQUEST = "Bad request"
    NOT_FOUND = "Not found"
    MISSING_TOKEN = "Token is missing"
    FORBIDDEN = "Authentication failed"


class UserPlan:
    """User plan values."""
    free = "free"
    subscriber = "subscriber"
    admin = "admin"


class EndpointSensitivity:
    """Authorized plans levels."""
    everyone = (UserPlan.free, UserPlan.subscriber, UserPlan.admin)
    subscribers = (UserPlan.subscriber, UserPlan.admin)
    protected = (UserPlan.admin,)


TOKEN_HEADER_KEY = "x-api-key"
CLIENT_SECRET_HEADER_KEY = "x-api-client-secret"

LAMBDA_CONTEXT_MESSAGE = "Calling context is %s"
INVALID_ID_PARAMETER = "Provided id is not convertible to int : %s"


class GatewayInputFields:
    """Fields to describe API Gateway lambda input."""
    headers = "headers"
    body = "body"
    qparameters = "queryStringParameters"
