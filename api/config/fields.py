class RootFields:
    """Common fields for various tables."""
    id = "id"
    name = "name"
    created_at = "created_at"
    updated_at = "updated_at"


class UserFields(RootFields):
    """Name of fields for users table."""
    mail = "email"
    password = "password"
    plan = "plan"

    # back-only fields
    salt = "salt"
    token = "token"


class DisciplineFields(RootFields):
    """Only subclass of RootFields for disciplines."""


class FamilyFields(DisciplineFields):
    """Fields for families table, inherits from discipline fields."""
    discipline = "discipline"
    discipline_id = "discipline_id"


class MovementFields(FamilyFields):
    """Fields for movements tables, inherits from family fields."""
    family = "family"
    family_id = "family_id"


class SessionFields(RootFields):
    """Fields for sessions."""
    date = "date"
    comment = "comment"

    user = "user"
    user_id = "user_id"
    is_draft = "is_draft"
    records = "records"

    date_range_filter = "date_range"


class RecordFields(RootFields):
    """Fields for records."""
    reps = "reps"
    weight = "weight"
    time = "time"
    execution = "execution"
    comment = "comment"

    movement = "movement"
    movement_id = "movement_id"
    session = "session"
    session_id = "session_id"
    user = "user"


class SecretFields(RootFields):
    """Fields for secrets."""
    token = "token"

    tokens = "tokens"
