"""Environment variables."""
from os import environ

# Database parameters
DATABASE_HOST = environ.get("DATABASE_HOST")
DATABASE_USER = environ.get("DATABASE_USER")
DATABASE_PWD = environ.get("DATABASE_PASSWORD")
DATABASE_NAME = environ.get("DATABASE_NAME", "postgres")
