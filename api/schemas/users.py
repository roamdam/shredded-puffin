from marshmallow import fields as mshm_fields, Schema


class GetUserSchema(Schema):
    """Schema for ``GET /user`` response."""
    id = mshm_fields.Integer()
    name = mshm_fields.String(required=True)
    email = mshm_fields.Email(required=True)
    plan = mshm_fields.String(required=True)
    token = mshm_fields.String()


class PutUserSchema(GetUserSchema):
    """Schema for ``PUT /user``.

    Contrary to other schemas, this one inherits from the ``GET /user`` schema
    with password added (we don't display password for get requests).
    """
    password = mshm_fields.String(required=True)


class PatchUserSchema(Schema):
    """Schema for ``PATCH /user/<id>``."""
    name = mshm_fields.String()
    email = mshm_fields.Email()
    password = mshm_fields.String()
    plan = mshm_fields.String()


class AuthUserSchema(Schema):
    """Schema for ``POST /user/auth``."""
    email = mshm_fields.Email(required=True)
    password = mshm_fields.String(required=True)
