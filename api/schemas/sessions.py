from crudance import PostInputSchema, PostOutputSchema
from marshmallow import fields as mshm_fields, Schema

from ..config.fields import SessionFields


class PutSessionSchema(Schema):
    """Schema for ``PUT /session``.

    ``user_id`` is automatically added from the authentication token by the
    controller.
    """
    id = mshm_fields.Integer(required=False)
    date = mshm_fields.String(required=True)
    name = mshm_fields.String(required=True, allow_none=True)
    comment = mshm_fields.String(required=True, allow_none=True)
    is_draft = mshm_fields.Boolean(required=True)


class GetSessionSchema(PutSessionSchema):
    """Schema for ``GET /session/<id>``.

    * same schema as ``PUT /session`` with id added
    * serialised records are added, but not required due to ``POST /session/search`` usage
    """
    SERIALIZED_PROPER_FIELDS = (
        SessionFields.id, SessionFields.date,
        SessionFields.name, SessionFields.comment, SessionFields.is_draft,
    )

    class SessionRecordSchema(Schema):
        """Schema for a serialised record."""
        class SessionRecordMovement(Schema):
            """Schema for a movement from a session's record."""
            id = mshm_fields.Integer(required=True)
            name = mshm_fields.String(required=True)

        id = mshm_fields.Integer(required=True)
        reps = mshm_fields.Integer(required=True)
        weight = mshm_fields.Float(required=True, allow_none=True)
        time = mshm_fields.Integer(required=True, allow_none=True)
        execution = mshm_fields.String(required=True, allow_none=True)
        comment = mshm_fields.String(required=True, allow_none=True)

        movement = mshm_fields.Nested(SessionRecordMovement, required=False)

    id = mshm_fields.Integer(required=True)
    records = mshm_fields.List(mshm_fields.Nested(SessionRecordSchema), required=False)


class PatchSessionSchema(Schema):
    """Schema for ``PATCH /session/<id>``.

    ``user_id`` cannot be patched, because it would attribute the session to
    other users.
    """
    date = mshm_fields.String()
    name = mshm_fields.String(allow_none=True)
    comment = mshm_fields.String(allow_none=True)
    is_draft = mshm_fields.Boolean()


class SearchSessionInputSchema(PostInputSchema):
    """Schema for ``POST /session/search`` input.

    As for ``POST /session``, user identification is made silently through the
    token, so does not appear in the input schema nor in the return fields.
    """
    DEFAULT_RETURN_FIELDS = (
        SessionFields.id, SessionFields.date,
        SessionFields.name, SessionFields.comment, SessionFields.is_draft,
    )

    class SearchSessionFilters(Schema):
        """Subschema for search session filters."""
        name = mshm_fields.String()
        date = mshm_fields.String()
        comment = mshm_fields.String()
        is_draft = mshm_fields.Boolean()
        date_range = mshm_fields.Nested(PostInputSchema.RangeFilterSchema)

    filters = mshm_fields.Nested(SearchSessionFilters, required=True)
    fields = mshm_fields.Constant(DEFAULT_RETURN_FIELDS, dump_only=True)


class SearchSessionOutputSchema(PostOutputSchema):
    """Schema for output of ``POST /session/search``."""
    data = mshm_fields.List(
        mshm_fields.Nested(GetSessionSchema),
        required=True
    )
