from marshmallow import Schema, fields as mshm_fields


class ShuffleSecretResponseSchema(Schema):
    """Response schema for ``POST /secret/shuffle``."""

    class ShuffledSecret(Schema):
        """Subschema for shuffled secret."""
        name = mshm_fields.String(required=True)
        token = mshm_fields.String(required=True)

    tokens = mshm_fields.List(mshm_fields.Nested(ShuffledSecret), required=True)
