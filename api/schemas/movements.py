from crudance import PostInputSchema, PostOutputSchema
from marshmallow import fields as mshm_fields, Schema

from ..config.fields import MovementFields
from .families import GetFamilySchema
from .disciplines import GetDisciplineSchema


class PutMovementSchema(Schema):
    """Schema for ``PUT /Movement``."""
    name = mshm_fields.String(required=True)
    family_id = mshm_fields.Integer(required=True)


class PatchMovementSchema(Schema):
    """Schema for ``PATCH /movement/<id>``."""
    name = mshm_fields.String()
    family_id = mshm_fields.Integer()


class GetMovementSchema(Schema):
    """Schema for a serialised Movement."""
    SERIALIZED_PROPER_FIELDS = MovementFields.id, MovementFields.name

    id = mshm_fields.Integer()
    name = mshm_fields.String(required=True)
    discipline = mshm_fields.Nested(GetDisciplineSchema, required=False)
    family = mshm_fields.Nested(GetFamilySchema, required=False)


class SearchMovementInputSchema(PostInputSchema):
    """Schema for ``POST /movement/search`` input."""
    DEFAULT_RETURN_FIELDS = (
        MovementFields.id, MovementFields.name,
        MovementFields.discipline, MovementFields.family
    )

    class SearchMovementFilters(Schema):
        """Subschema for search Movement filters."""

        class MovementDisciplineJoinFilter(Schema):
            """Subfilter for join with discipline table."""
            id = mshm_fields.Integer()
            name = mshm_fields.String()

        class MovementFamilyJoinFilter(Schema):
            """Subfilter for join with families table."""
            id = mshm_fields.Integer()
            name = mshm_fields.String()

        name = mshm_fields.String()
        discipline = mshm_fields.Nested(MovementDisciplineJoinFilter)
        family = mshm_fields.Nested(MovementFamilyJoinFilter)

    filters = mshm_fields.Nested(SearchMovementFilters, required=True)
    fields = mshm_fields.Constant(DEFAULT_RETURN_FIELDS, dump_only=True)


class SearchMovementOutputSchema(PostOutputSchema):
    """Schema for output of ``POST /Movement/search``."""
    data = mshm_fields.List(
        mshm_fields.Nested(GetMovementSchema),
        required=True
    )
