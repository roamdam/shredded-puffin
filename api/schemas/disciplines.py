from crudance import PostInputSchema, PostOutputSchema
from marshmallow import fields as mshm_fields, Schema

from ..config.fields import DisciplineFields


class PutDisciplineSchema(Schema):
    """Schema for ``PUT /discipline``."""
    name = mshm_fields.String(required=True)


class PatchDisciplineSchema(Schema):
    """Schema for ``PATCH /discipline/<id>``."""
    name = mshm_fields.String()


class GetDisciplineSchema(PutDisciplineSchema):
    """Schema for a serialised discipline."""
    id = mshm_fields.Integer()


class SearchDisciplineInputSchema(PostInputSchema):
    """Schema for ``POST /discipline/search`` input."""
    DEFAULT_RETURN_FIELDS = (DisciplineFields.id, DisciplineFields.name)

    class SearchDisciplineFilters(Schema):
        """Subschema for search discipline filters."""
        name = mshm_fields.String()

    filters = mshm_fields.Nested(SearchDisciplineFilters, required=True)
    fields = mshm_fields.Constant(DEFAULT_RETURN_FIELDS, dump_only=True)


class SearchDisciplineOutputSchema(PostOutputSchema):
    """Schema for output of ``POST /discipline/search``."""
    data = mshm_fields.List(
        mshm_fields.Nested(GetDisciplineSchema),
        required=True
    )
