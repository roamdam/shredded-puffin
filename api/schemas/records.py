from crudance import PostInputSchema, PostOutputSchema
from marshmallow import fields as mshm_fields, Schema

from ..config.fields import RecordFields
from .movements import GetMovementSchema
from .sessions import GetSessionSchema


class PatchRecordSchema(Schema):
    """Schema for ``PATCH /record/<record_id>``."""
    reps = mshm_fields.Integer()
    weight = mshm_fields.Float(allow_none=True)
    time = mshm_fields.Integer(allow_none=True)
    execution = mshm_fields.String(allow_none=True)
    comment = mshm_fields.String(allow_none=True)

    session_id = mshm_fields.Integer()
    movement_id = mshm_fields.Integer()


class PutRecordSchema(Schema):
    """Schema for ``PUT /record``."""
    id = mshm_fields.Integer(required=False)
    reps = mshm_fields.Integer(required=True)
    weight = mshm_fields.Float(required=True, allow_none=True)
    time = mshm_fields.Integer(required=True, allow_none=True)
    execution = mshm_fields.String(required=True, allow_none=True)
    comment = mshm_fields.String(required=True, allow_none=True)

    movement_id = mshm_fields.Integer(required=True)
    session_id = mshm_fields.Integer(required=True)


class PostRecordsSchema(Schema):
    """Schema to post records by batch."""
    records = mshm_fields.List(mshm_fields.Nested(PutRecordSchema), required=True)


class GetRecordSchema(Schema):
    """Schema for a serialised record."""
    SERIALIZED_PROPER_FIELDS = (
        RecordFields.id,
        RecordFields.reps,
        RecordFields.weight,
        RecordFields.time,
        RecordFields.execution,
        RecordFields.comment
    )
    id = mshm_fields.Integer(required=True)
    reps = mshm_fields.Integer(required=True)
    weight = mshm_fields.Float(required=True, allow_none=True)
    time = mshm_fields.Integer(required=True, allow_none=True)
    execution = mshm_fields.String(required=True, allow_none=True)
    comment = mshm_fields.String(required=True, allow_none=True)

    session = mshm_fields.Nested(GetSessionSchema, required=False)
    movement = mshm_fields.Nested(GetMovementSchema, required=False)


class SearchRecordInputSchema(PostInputSchema):
    """Schema for ``POST /record/search`` input."""
    DEFAULT_RETURN_FIELDS = (
        RecordFields.id,
        RecordFields.reps,
        RecordFields.weight,
        RecordFields.time,
        RecordFields.execution,
        RecordFields.comment,
        RecordFields.session,
        RecordFields.movement
    )

    class SearchRecordFilters(Schema):
        """Subschema for search record filters."""

        class RecordMovementJoinFilter(Schema):
            """Subfilter for join with movements."""
            id = mshm_fields.Integer()
            name = mshm_fields.String()

        class RecordSessionJoinFilter(Schema):
            """Subfilter for join with sessions."""
            id = mshm_fields.Integer()
            name = mshm_fields.String()
            date = mshm_fields.String()

        session = mshm_fields.Nested(RecordSessionJoinFilter)
        movement = mshm_fields.Nested(RecordMovementJoinFilter)
        reps = mshm_fields.Integer()
        weight = mshm_fields.Float()
        time = mshm_fields.Integer()
        execution = mshm_fields.String()
        comment = mshm_fields.String()

    filters = mshm_fields.Nested(SearchRecordFilters, required=True)
    fields = mshm_fields.Constant(DEFAULT_RETURN_FIELDS, dump_only=True)


class SearchRecordOutputSchema(PostOutputSchema):
    """Schema for output of ``POST /record/search``."""
    data = mshm_fields.List(
        mshm_fields.Nested(GetRecordSchema),
        required=True
    )
