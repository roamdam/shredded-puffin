from crudance import PostInputSchema, PostOutputSchema
from marshmallow import fields as mshm_fields, Schema

from ..config.fields import FamilyFields
from .disciplines import GetDisciplineSchema


class PutFamilySchema(Schema):
    """Schema for ``PUT /family``."""
    name = mshm_fields.String(required=True)
    discipline_id = mshm_fields.Integer(required=True)


class PatchFamilySchema(Schema):
    """Schema for ``PATCH /family/<id>``."""
    name = mshm_fields.String()
    discipline_id = mshm_fields.Integer()


class GetFamilySchema(Schema):
    """Schema for a serialised Family."""
    id = mshm_fields.Integer()
    name = mshm_fields.String(required=True)
    discipline = mshm_fields.Nested(GetDisciplineSchema, required=False)


class SearchFamilyInputSchema(PostInputSchema):
    """Schema for ``POST /family/search`` input."""
    DEFAULT_RETURN_FIELDS = (FamilyFields.id, FamilyFields.name, FamilyFields.discipline)

    class SearchFamilyFilters(Schema):
        """Subschema for search Family filters."""

        class FamilyDisciplineJoinFilter(Schema):
            """Subfilter for join with disciplines table."""
            id = mshm_fields.Integer()
            name = mshm_fields.String()

        name = mshm_fields.String()
        discipline = mshm_fields.Nested(FamilyDisciplineJoinFilter)

    filters = mshm_fields.Nested(SearchFamilyFilters, required=True)
    fields = mshm_fields.Constant(DEFAULT_RETURN_FIELDS, dump_only=True)


class SearchFamilyOutputSchema(PostOutputSchema):
    """Schema for output of ``POST /Family/search``."""
    data = mshm_fields.List(
        mshm_fields.Nested(GetFamilySchema),
        required=True
    )
