"""ORM engine and session creation."""
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy.pool import NullPool

from ..config.settings import database_uri


engine = create_engine(f"{database_uri}", echo=True, poolclass=NullPool)
db_session = Session(engine)
