"""Database model schema."""
from sqlalchemy import (
    Column, Date, DateTime, Float, ForeignKey, Integer, String, text, Enum, Boolean,
    UniqueConstraint
)
from sqlalchemy.orm import declarative_base, relationship


Base = declarative_base()
metadata = Base.metadata


class Constants:
    text_false = text("false")
    text_now = text("now()")
    text_empty = text("''::character varying")


class Enums:
    user_plan = Enum(
        "free",
        "subscriber",
        "admin",
        name='user_plan_enum'
    )


class Discipline(Base):
    __tablename__ = 'disciplines'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    created_at = Column(DateTime, nullable=False, server_default=Constants.text_now)
    updated_at = Column(DateTime, nullable=False, server_default=Constants.text_now)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    email = Column(String, nullable=False, unique=True, index=True)

    password = Column(String, nullable=False)
    salt = Column(String, nullable=False)
    token = Column(String, unique=True, nullable=False, index=True)
    plan = Column(Enums.user_plan, nullable=False, server_default=text("'free'::user_plan_enum"))

    created_at = Column(DateTime, nullable=False, server_default=Constants.text_now)
    updated_at = Column(DateTime, nullable=False, server_default=Constants.text_now)


class Family(Base):
    __tablename__ = 'families'
    __table_args__ = (
        UniqueConstraint('name', 'discipline_id'),
    )

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    discipline_id = Column(ForeignKey('disciplines.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False)

    discipline = relationship('Discipline')


class Session(Base):
    __tablename__ = 'sessions'

    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False, server_default=Constants.text_now)
    name = Column(String)
    comment = Column(String)
    is_draft = Column(Boolean, nullable=False, server_default=Constants.text_false)
    user_id = Column(ForeignKey('users.id'), nullable=False)

    user = relationship('User')


class Movement(Base):
    __tablename__ = 'movements'
    __table_args__ = (
        UniqueConstraint('name', 'family_id'),
    )

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    family_id = Column(ForeignKey('families.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False)

    family = relationship('Family')


class Record(Base):
    __tablename__ = 'records'

    id = Column(Integer, primary_key=True)
    session_id = Column(ForeignKey('sessions.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    movement_id = Column(ForeignKey('movements.id'), nullable=False, index=True)
    reps = Column(Integer, nullable=False)
    weight = Column(Float)
    time = Column(Integer)
    execution = Column(String)
    comment = Column(String)

    movement = relationship('Movement')
    session = relationship('Session')


class Secret(Base):
    __tablename__ = 'secrets'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    token = Column(String, nullable=False, unique=True, index=True)
    created_at = Column(DateTime, nullable=False, server_default=Constants.text_now)
    updated_at = Column(DateTime, nullable=False, server_default=Constants.text_now)
