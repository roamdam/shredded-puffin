from functools import wraps
from http import HTTPStatus

from marshmallow import ValidationError, Schema

from ..config.definitions import HTTPMessages, GatewayInputFields as fields


def schema_shield(schema: Schema) -> dict:
    """Execute lambda handler and abort with 400 in case of a schema ``ValidationError``.

    The decorated function must be a lambda handler with ``event`` and ``context``
    parameters. Body to validate is taken from ``event`` parameter, ``context`` is
    passed as is to the lambda handler.
    """
    def cover(fun):

        @wraps(fun)
        def decorated(event, context):
            try:
                schema().loads(event[fields.body])
            except KeyError:
                print("Input event does not contain the required body.")
                return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
            except ValidationError as e:
                print(f"Schema error when calling {fun} : {e.messages}")
                return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

            return fun(event, context)
        return decorated
    return cover


def query_string_shield(*args) -> dict:
    """Check that lambda handler's event contains the required query string parameters.

    Required parameters are given as string positional arguments (no keyword args).

    The decorated function must be a lambda handler with ``event`` and ``context``
    parameters. Body to validate is taken from ``event`` parameter, ``context`` is
    passed as is to the lambda handler.
    """
    def cover(fun):

        @wraps(fun)
        def decorated(event, context):
            try:
                parameters = event[fields.qparameters]
            except KeyError:
                print("No query string parameters provided")
                return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

            for required_parameter in args:
                try:
                    parameters[required_parameter]
                except KeyError:
                    print(f"Missing required parameter {required_parameter}")
                    return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

            return fun(event, context)
        return decorated
    return cover
