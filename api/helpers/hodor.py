from functools import wraps
from hashlib import pbkdf2_hmac
from http import HTTPStatus
from logging import getLogger
from secrets import token_hex
from typing import Union

from sqlalchemy.exc import NoResultFound

from api.config.definitions import (
    HTTPMessages, GatewayInputFields as fields,
    TOKEN_HEADER_KEY, CLIENT_SECRET_HEADER_KEY, EndpointSensitivity
)
from api.model.engine import db_session
from api.model.schema import User, Secret


class Hodor:
    """Password and tokens helpers.

    It holds the door to the database.
    """
    SALT_SIZE = 32
    TOKEN_SIZE = 32
    HASH_DIGEST_ALGORITHM = 'sha256'
    HASH_NB_ITERATIONS = 10000
    HASH_KEY_SIZE = 128
    MIN_PASSWORD_SIZE = 12

    def __init__(self) -> None:
        self.session = db_session
        self.logger = getLogger(__name__)

    def find_element_from_token(self, token: str, table: Union[User, Secret] = User) -> Union[User, Secret]:
        """Find the one element from given token."""
        self.logger.debug("Look for %s from token %s", table, token)
        return self.session.query(table).filter(table.token == token).one()

    @staticmethod
    def generate_salt():
        """Generate salt."""
        return token_hex(Hodor.SALT_SIZE)

    @staticmethod
    def generate_token():
        """Generate token."""
        return token_hex(Hodor.TOKEN_SIZE)

    @staticmethod
    def encrypt_password(salt: str, password: str) -> str:
        """Encrypt salted password."""
        return str(pbkdf2_hmac(
            hash_name=Hodor.HASH_DIGEST_ALGORITHM,
            password=password.encode('utf-8'),
            salt=salt.encode('utf-8'),
            iterations=Hodor.HASH_NB_ITERATIONS,
            dklen=Hodor.HASH_KEY_SIZE
        ))

    @staticmethod
    def is_password_strong(password: str) -> bool:
        """Check that a given password is long enough."""
        return len(password) >= Hodor.MIN_PASSWORD_SIZE


def token_shield(sensitivity: tuple = EndpointSensitivity.protected) -> dict:
    """Protect endpoint with token and plan verification.

    The decorated function must be a lambda handler with ``event`` and ``context``
    parameters. Headers are taken from ``event`` parameter, ``context`` is passed
    as is to the lambda handler.

    By default, endpoint is protected with admin level : only admin user are
    authorized.
    """
    hodor = Hodor()

    def check_token(fun):

        @wraps(fun)
        def decorated(event, context):
            headers = event[fields.headers]
            token = headers.get(TOKEN_HEADER_KEY)
            secret = headers.get(CLIENT_SECRET_HEADER_KEY)

            if token is None or secret is None:
                # No token provided, request is unauthorized
                print(f"No token provided for call to {fun} (headers {headers})")
                return {"message": HTTPMessages.MISSING_TOKEN, "status": HTTPStatus.UNAUTHORIZED}

            # Check client secret first
            try:
                hodor.find_element_from_token(token=secret, table=Secret)
            except NoResultFound:
                print(f"No secret found for token {token}")
                return {"message": HTTPMessages.FORBIDDEN, "status": HTTPStatus.FORBIDDEN}

            # Token provided, find corresponding (unique) user
            try:
                user = hodor.find_element_from_token(token=token, table=User)
            except NoResultFound:
                print("No user found for token {token}")
                return {"message": HTTPMessages.FORBIDDEN, "status": HTTPStatus.FORBIDDEN}

            if user.plan not in sensitivity:
                print(f"User {user.id} has not sufficient privileges for method {fun}")
                return {"message": HTTPMessages.FORBIDDEN, "status": HTTPStatus.FORBIDDEN}

            return fun(event, context)
        return decorated
    return check_token
