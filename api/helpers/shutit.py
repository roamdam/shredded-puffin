from functools import wraps
from traceback import format_exc

from api.model.engine import db_session


def shut_session(fun) -> dict:
    """Execute decorated function then close all methods.

    Must be the last internal applied decorator otherwise it will shut connexions
    before the other decorators can be applied.
    """
    @wraps(fun)
    def decorated(*args, **kwargs):
        result = fun(*args, **kwargs)
        try:
            db_session.close()
        except Exception:
            print(f"Unable to close db session : {format_exc()}")

        return result
    return decorated
