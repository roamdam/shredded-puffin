from logging import getLogger

from crudance import PostSearchControllerTemplate
from sqlalchemy.orm import Query

from api.config.fields import UserFields, SessionFields
from api.model.engine import db_session
from api.model.schema import Session, User
from api.schemas.sessions import SearchSessionInputSchema


class SearchSessionController(PostSearchControllerTemplate):
    """Search controller for session ``POST /session/search``."""
    JOINS_FILTERS = [
        {
            "slot": SessionFields.user,
            "table": User,
            "map_method": "map_user"
        }
    ]
    RANGE_FILTERS = [
        {
            "slot": SessionFields.date_range_filter,
            "column": Session.date,
            "is_date": True
        }
    ]
    DATE_FIELDS = [SessionFields.date]
    LOOSE_STRING_FIELDS = [SessionFields.name, SessionFields.comment]

    def __init__(self, user: User) -> None:
        super().__init__(target=Session, session=db_session)
        self.user = user
        self.logger = getLogger(__name__)

    def search(self, **kwargs) -> dict:
        """Supercharge search.

        * add default return fields if missing
        * add user if silent filter

        Serialisation is not supercharged since we do not need to serialise user
        data.
        """
        if kwargs.get("fields") is None:
            self.logger.debug("Set fields to default when missing from input")
            kwargs["fields"] = SearchSessionInputSchema.DEFAULT_RETURN_FIELDS

        self.logger.debug("Add silent filter on authenticated user %s", self.user.id)
        kwargs["filters"][SessionFields.user] = {
            UserFields.id: self.user.id
        }

        return super().search(**kwargs)

    def map_user(self, partial: Query, filters: dict) -> Query:
        """Map join with users table."""
        return partial.join(User)
