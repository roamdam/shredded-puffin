from logging import getLogger

from crudance import GetterDeleterTemplate

from api.config.fields import SessionFields, RecordFields
from api.controllers.records.search_record_controller import SearchRecordController
from api.model.engine import db_session
from api.model.schema import Session, Record
from api.schemas.records import GetRecordSchema
from api.schemas.sessions import GetSessionSchema


class GetSessionController(GetterDeleterTemplate):
    """Controller for ``GET /session`` and ``DELETE /session``."""
    IDENTIFYING_FIELDS = [SessionFields.id]
    TARGET_FIELDS = GetSessionSchema.SERIALIZED_PROPER_FIELDS
    DATE_FIELDS = [SessionFields.date]

    def __init__(self) -> None:
        super().__init__(target=Session, session=db_session)
        self.record_controller = SearchRecordController(user=None)
        self.logger = getLogger(__name__)

    def _serialize(self, element: Session, selected_fields: list) -> dict:
        # Serialize session normally
        serialized_session = super()._serialize(element=element, selected_fields=selected_fields)

        # Add records for the session
        serialized_session[SessionFields.records] = []
        found_records = self.session.query(Record).filter(Record.session_id == element.id)
        # Prepare record serialized fields
        records_target_fields = list(GetRecordSchema.SERIALIZED_PROPER_FIELDS)
        records_target_fields.append(RecordFields.movement)
        for session_record in found_records:
            serialized_record = self.record_controller._serialize(
                element=session_record,
                selected_fields=records_target_fields
            )
            serialized_session[SessionFields.records].append(serialized_record)

        return serialized_session
