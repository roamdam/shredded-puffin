from logging import getLogger

from crudance import InsertMapperTemplate

from api.config.fields import SessionFields
from api.model.engine import db_session
from api.model.schema import Session, User


class PutPatchSessionController(InsertMapperTemplate):
    """Controller for adding or updating sessions.

    User is linked directly from authenticated user for session creation.
    """
    IDENTIFYING_FIELDS = [SessionFields.id]
    DATE_FIELDS = [SessionFields.date]

    def __init__(self, user: User) -> None:
        super().__init__(target=Session, session=db_session)
        self.user = user
        self.logger = getLogger(__name__)

    def put(self, body: dict) -> int:
        """Supercharge put method to add user identfication."""
        self.logger.debug("Add authenticated user to put body.")
        body[SessionFields.user_id] = self.user.id
        self.logger.debug("PUT augmented session body : %s", body)
        return super().put(body=body)
