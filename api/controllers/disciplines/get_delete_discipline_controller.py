from logging import getLogger

from crudance import GetterDeleterTemplate

from api.config.fields import DisciplineFields
from api.model.engine import db_session
from api.model.schema import Discipline


class GetDisciplineController(GetterDeleterTemplate):
    """Controller for ``GET /discipline`` and ``DELETE /discipline``."""
    IDENTIFYING_FIELDS = [DisciplineFields.id]
    TARGET_FIELDS = [DisciplineFields.name]
    DATE_FIELDS = [DisciplineFields.created_at, DisciplineFields.updated_at]

    def __init__(self) -> None:
        super().__init__(target=Discipline, session=db_session)
        self.logger = getLogger(__name__)
