from logging import getLogger

from crudance import InsertMapperTemplate

from api.config.fields import DisciplineFields
from api.model.engine import db_session
from api.model.schema import Discipline


class PutPatchDisciplineController(InsertMapperTemplate):
    """Controller for PUT and PATCH Discipline."""
    IDENTIFYING_FIELDS = [DisciplineFields.id]

    def __init__(self) -> None:
        super().__init__(target=Discipline, session=db_session)
        self.logger = getLogger(__name__)
