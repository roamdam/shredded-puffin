from logging import getLogger

from crudance import PostSearchControllerTemplate

from api.config.fields import DisciplineFields
from api.model.engine import db_session
from api.model.schema import Discipline
from api.schemas.disciplines import SearchDisciplineInputSchema


class SearchDisciplineController(PostSearchControllerTemplate):
    """Search controller for discipline ``POST /discipline/search``."""
    DATE_FIELDS = [DisciplineFields.created_at, DisciplineFields.updated_at]

    def __init__(self) -> None:
        super().__init__(target=Discipline, session=db_session)
        self.logger = getLogger(__name__)

    def search(self, **kwargs) -> dict:
        """Supercharge search to add default return fields if missing."""
        if kwargs.get("fields") is None:
            kwargs["fields"] = SearchDisciplineInputSchema.DEFAULT_RETURN_FIELDS

        return super().search(**kwargs)
