from logging import getLogger

from crudance import InsertMapperTemplate

from api.config.fields import RecordFields
from api.model.engine import db_session
from api.model.schema import Record


class PutPatchRecordController(InsertMapperTemplate):
    """Controller for PUT and PATCH record."""
    IDENTIFYING_FIELDS = [RecordFields.id]

    def __init__(self) -> None:
        super().__init__(target=Record, session=db_session)
        self.logger = getLogger(__name__)
