from logging import getLogger

from crudance import GetterDeleterTemplate

from api.config.fields import RecordFields
from api.model.engine import db_session
from api.model.schema import Record


class GetRecordController(GetterDeleterTemplate):
    """Controller for ``GET /record`` and ``DELETE /record``."""
    IDENTIFYING_FIELDS = [RecordFields.id]
    TARGET_FIELDS = [
        RecordFields.reps,
        RecordFields.weight,
        RecordFields.time,
        RecordFields.execution,
        RecordFields.comment
    ]

    def __init__(self) -> None:
        super().__init__(target=Record, session=db_session)
        self.logger = getLogger(__name__)
