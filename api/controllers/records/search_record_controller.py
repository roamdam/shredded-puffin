from logging import getLogger

from crudance import PostSearchControllerTemplate
from crudance.config.fields import PostInputFields
from sqlalchemy.orm import Query

from api.config.fields import RecordFields, UserFields, SessionFields
from api.model.engine import db_session
from api.model.schema import Record, Movement, Session, User
from api.schemas.movements import GetMovementSchema
from api.schemas.records import SearchRecordInputSchema
from api.schemas.sessions import GetSessionSchema


class SearchRecordController(PostSearchControllerTemplate):
    """Search controller for Record ``POST /record/search``."""
    JOINS_FILTERS = [
        {
            "slot": RecordFields.session,
            "table": Session,
            "map_method": "map_session"
        },
        {
            "slot": RecordFields.movement,
            "table": Movement,
            "map_method": "map_movement"
        },
        {
            "slot": RecordFields.user,
            "table": User,
            "map_method": "map_user"
        }
    ]
    DATE_FIELDS = [SessionFields.date]
    LOOSE_STRING_FIELDS = [RecordFields.execution, RecordFields.comment]

    def __init__(self, user: User) -> None:
        super().__init__(target=Record, session=db_session)
        self.user = user
        self.logger = getLogger(__name__)

    def search(self, **kwargs) -> dict:
        """Supercharge search.

        * add default return fields if missing
        * add user if silent filter
        """
        if kwargs.get("fields") is None:
            # Set fields to default when missing from input
            kwargs["fields"] = SearchRecordInputSchema.DEFAULT_RETURN_FIELDS

        # Add silent filter on authenticated user
        kwargs["filters"][RecordFields.user] = {
            UserFields.id: self.user.id
        }

        return super().search(**kwargs)

    def map_session(self, partial: Query, filters: dict) -> Query:
        """Map join with sessions table."""
        return partial.join(Session)

    def map_movement(self, partial: Query, filters: dict) -> Query:
        """Map join with movement table."""
        return partial.join(Movement)

    def map_user(self, partial: Query, filters: dict) -> Query:
        """Map user filter using session ids."""
        session_sub = self._apply_filters_clause(
            partial=self.session.query(Session).join(User),
            target=User,
            filters=filters.pop(RecordFields.user),
            operator=PostInputFields.Operators.and_
        ).subquery("session_join")

        return partial.join(session_sub, session_sub.c.id == Record.session_id)

    def _serialize(self, element: Record, selected_fields: list) -> dict:
        """Supercharge serialize to add session and movement elements."""
        record = super()._serialize(element=element, selected_fields=selected_fields)

        # Serialize session only if asked
        if RecordFields.session in selected_fields:
            # Get corresponding session
            session = self.session.query(Session).get(element.session_id)
            record[RecordFields.session] = super()._serialize(
                element=session,
                selected_fields=GetSessionSchema.SERIALIZED_PROPER_FIELDS
            )
        # Serialize movement only if asked
        if RecordFields.movement in selected_fields:
            # Get corresponding movement
            movement = self.session.query(Movement).get(element.movement_id)
            record[RecordFields.movement] = super()._serialize(
                element=movement,
                selected_fields=GetMovementSchema.SERIALIZED_PROPER_FIELDS
            )
        return record
