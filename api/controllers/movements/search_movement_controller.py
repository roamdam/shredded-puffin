from logging import getLogger

from crudance import PostSearchControllerTemplate
from crudance.config.fields import PostInputFields
from sqlalchemy.orm import Query

from api.config.fields import MovementFields, FamilyFields, DisciplineFields
from api.model.engine import db_session
from api.model.schema import Movement, Discipline, Family
from api.schemas.movements import SearchMovementInputSchema


class SearchMovementController(PostSearchControllerTemplate):
    """Search controller for Movement ``POST /movement/search``."""
    JOINS_FILTERS = [
        {
            "slot": MovementFields.discipline,
            "table": Discipline,
            "map_method": "map_discipline"
        },
        {
            "slot": MovementFields.family,
            "table": Family,
            "map_method": "map_family"
        }
    ]

    def __init__(self) -> None:
        super().__init__(target=Movement, session=db_session)
        self.logger = getLogger(__name__)

    def search(self, **kwargs) -> dict:
        """Supercharge search to add default return fields if missing."""
        if kwargs.get("fields") is None:
            kwargs["fields"] = SearchMovementInputSchema.DEFAULT_RETURN_FIELDS

        return super().search(**kwargs)

    def map_discipline(self, partial: Query, filters: dict) -> Query:
        """Map join with disciplines table."""
        # Subquery to find family ids corresponding to given discipline
        discipline_query = self._apply_filters_clause(
            partial=self.session.query(Family).join(Discipline),
            target=Discipline,
            filters=filters.pop(MovementFields.discipline),
            operator=PostInputFields.Operators.and_
        ).subquery("discipline_join")

        return partial.join(discipline_query, discipline_query.c.id == Movement.family_id)

    def map_family(self, partial: Query, filters: dict) -> Query:
        """Map join with family table."""
        return partial.join(Family)

    def _serialize(self, element: Movement, selected_fields: list) -> dict:
        """Supercharge serialize to add discipline and family elements."""
        movement = super()._serialize(element=element, selected_fields=selected_fields)

        # Serialize discipline only if asked
        if MovementFields.discipline in selected_fields:
            # Get corresponding discipline
            discipline = (
                self.session.query(Discipline)
                .join(Family)
                .filter(Family.id == element.family_id)
                .first()
            )
            movement[MovementFields.discipline] = super()._serialize(
                element=discipline,
                selected_fields=[DisciplineFields.id, DisciplineFields.name]
            )
        # Serialize familiy only if asked
        if MovementFields.family in selected_fields:
            # Get corresponding family
            family = self.session.query(Family).get(element.family_id)
            movement[MovementFields.family] = super()._serialize(
                element=family,
                selected_fields=[FamilyFields.id, FamilyFields.name]
            )
        return movement
