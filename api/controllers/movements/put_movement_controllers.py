from logging import getLogger

from crudance import InsertMapperTemplate

from api.config.fields import MovementFields
from api.model.engine import db_session
from api.model.schema import Movement


class PutPatchMovementController(InsertMapperTemplate):
    """Controller for PUT and PATCH movement."""
    IDENTIFYING_FIELDS = [MovementFields.id]

    def __init__(self) -> None:
        super().__init__(target=Movement, session=db_session)
        self.logger = getLogger(__name__)
