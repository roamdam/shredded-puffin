from logging import getLogger

from crudance import GetterDeleterTemplate

from api.config.fields import MovementFields
from api.model.engine import db_session
from api.model.schema import Movement


class GetMovementController(GetterDeleterTemplate):
    """Controller for ``GET /movement`` and ``DELETE /movement``."""
    IDENTIFYING_FIELDS = [MovementFields.id]
    TARGET_FIELDS = [MovementFields.name]

    def __init__(self) -> None:
        super().__init__(target=Movement, session=db_session)
        self.logger = getLogger(__name__)
