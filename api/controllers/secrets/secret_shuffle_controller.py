from datetime import datetime
from logging import getLogger
from typing import List

from crudance.helpers.within_transaction import within_transaction

from api.config.fields import SecretFields
from api.helpers.hodor import Hodor
from api.model.engine import db_session
from api.model.schema import Secret


class SecretShuffleController:
    """Controller to shuffle all secrets."""

    def __init__(self) -> None:
        self.session = db_session
        self.logger = getLogger(__name__)

    def shuffle_tokens(self) -> List[dict]:
        """Shuffle tokens and return generated values."""
        return within_transaction(self.session, self._shuffle_tokens)()

    def _shuffle_tokens(self) -> List[dict]:
        """Shuffle each secret's token and append to result list."""
        tokens = []
        for secret in self._get_secrets():
            token = self._shuffle_token(secret)
            tokens.append({
                SecretFields.name: secret.name,
                SecretFields.token: token
            })
        return tokens

    def _get_secrets(self):
        """Yield all secrets."""
        yield from self.session.query(Secret).all()

    @staticmethod
    def _shuffle_token(secret: Secret) -> str:
        """Shuffle token from secret and return it."""
        token = Hodor.generate_token()
        setattr(secret, SecretFields.token, token)
        setattr(secret, SecretFields.updated_at, datetime.now())
        return token
