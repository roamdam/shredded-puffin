from logging import getLogger

from crudance import GetterDeleterTemplate

from api.config.fields import UserFields
from api.model.engine import db_session
from api.model.schema import User


class GetUserController(GetterDeleterTemplate):
    """Controller for ``GET /user`` and ``DELETE /user``."""
    IDENTIFYING_FIELDS = [UserFields.id]
    TARGET_FIELDS = [UserFields.name, UserFields.mail, UserFields.plan, UserFields.token]
    DATE_FIELDS = [UserFields.created_at, UserFields.updated_at]

    def __init__(self) -> None:
        super().__init__(target=User, session=db_session)
        self.logger = getLogger(__name__)
