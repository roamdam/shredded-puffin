from logging import getLogger

from crudance import InsertMapperTemplate

from api.config.fields import UserFields
from api.helpers.hodor import Hodor
from api.model.engine import db_session
from api.model.schema import User


class PutPatchUserController(InsertMapperTemplate):
    """Controller for PUT and PATCH user."""
    IDENTIFYING_FIELDS = [UserFields.id]

    def __init__(self) -> None:
        super().__init__(target=User, session=db_session)
        self.hodor = Hodor()
        self.logger = getLogger(__name__)

    def put(self, body: dict) -> int:
        """Create user with encrypted password and generate api token."""
        self.logger.debug("Expand auth values before creating user.")
        self._expand_body_with_auth(body=body)
        self.logger.debug("Write new user with expanded auth values.")
        return super().put(body)

    def patch(self, body: dict) -> None:
        """Update given user infos.

        Update salt and api token if there is a password change.
        """
        if UserFields.password in body.keys():
            self.logger.debug("Patch password, expand auth values first.")
            self._expand_body_with_auth(body)

        self.logger.debug("Patch user with expanded auth values.")
        return super().patch(body)

    def _expand_body_with_auth(self, body: dict) -> None:
        """Add authentification variables to body."""
        self.logger.debug("Generate auth variables and append to user body.")
        # Generate encryption items
        salt = self.hodor.generate_salt()
        token = self.hodor.generate_token()
        encrypted_password = self.hodor.encrypt_password(salt=salt, password=body[UserFields.password])

        # Add encryption items
        body[UserFields.salt] = salt
        body[UserFields.token] = token
        body[UserFields.password] = encrypted_password
