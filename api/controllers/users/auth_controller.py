from logging import getLogger

from sqlalchemy.exc import NoResultFound, MultipleResultsFound

from api.config.fields import UserFields
from api.model.engine import db_session
from api.model.schema import User
from api.helpers.hodor import Hodor
from .get_delete_user_controller import GetUserController
from .put_user_controllers import PutPatchUserController


class IncorrectCredentials(ValueError):
    """Any type of authentication failure."""
    pass


class AuthUserController:
    """Auth controller, uses both patcher and getter."""

    def __init__(self) -> None:
        self.session = db_session
        self.patcher = PutPatchUserController()
        self.getter = GetUserController()
        self.hodor = Hodor()
        self.logger = getLogger(__name__)

    def authenticate(self, email: str, password: str) -> dict:
        """Check user credentials and return updated api token."""
        try:
            user = self._find_user_from_credentials(email=email, password=password)
        except (NoResultFound, MultipleResultsFound):
            self.logger.debug("Unable to identify user %s from email", email)
            raise IncorrectCredentials("Unable to identify user from email")
        else:
            self.logger.debug("User %s authenticates, shuffle salt and token", email)
            patch_auth = {UserFields.id: user.id, UserFields.password: password}

        self.logger.info("Patch user %s auth infos", user.id)
        self.patcher.patch(patch_auth)
        return self.getter.get(id=user.id)

    def logout(self, token: str) -> None:
        """Log out user : shuffle its token."""
        user = self.hodor.find_element_from_token(token=token, table=User)
        unauth_body = {UserFields.id: user.id, UserFields.token: self.hodor.generate_token()}
        return self.patcher.patch(unauth_body)

    def _find_user_from_credentials(self, email: str, password: str) -> User:
        """Find user from its email, then password, raising if incorrect."""
        user = self.session.query(User).filter(User.email == email).one()
        encrypted_given_password = self.hodor.encrypt_password(salt=user.salt, password=password)
        if encrypted_given_password != user.password:
            self.logger.debug("Passwords do not match for user %s", email)
            raise IncorrectCredentials("Passwords do not match")

        return user
