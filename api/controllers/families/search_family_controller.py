from logging import getLogger

from crudance import PostSearchControllerTemplate
from sqlalchemy.orm import Query

from api.config.fields import FamilyFields, DisciplineFields
from api.model.engine import db_session
from api.model.schema import Discipline, Family
from api.schemas.families import SearchFamilyInputSchema


class SearchFamilyController(PostSearchControllerTemplate):
    """Search controller for family ``POST /family/search``."""
    JOINS_FILTERS = [
        {
            "slot": FamilyFields.discipline,
            "table": Discipline,
            "map_method": "map_discipline"
        }
    ]

    def __init__(self) -> None:
        super().__init__(target=Family, session=db_session)
        self.logger = getLogger(__name__)

    def search(self, **kwargs) -> dict:
        """Supercharge search to add default return fields if missing."""
        if kwargs.get("fields") is None:
            kwargs["fields"] = SearchFamilyInputSchema.DEFAULT_RETURN_FIELDS

        return super().search(**kwargs)

    def map_discipline(self, partial: Query, filters: dict) -> Query:
        """Map join with disciplines table."""
        return partial.join(Discipline)

    def _serialize(self, element: Family, selected_fields: list) -> dict:
        family = super()._serialize(element=element, selected_fields=selected_fields)

        # Serialize discipline only if asked
        if FamilyFields.discipline in selected_fields:
            # Get corresponding discipline
            discipline = self.session.query(Discipline).get(element.discipline_id)
            family[FamilyFields.discipline] = super()._serialize(
                element=discipline,
                selected_fields=[DisciplineFields.id, DisciplineFields.name]
            )
        return family
