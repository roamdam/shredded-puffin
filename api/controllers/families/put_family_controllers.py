from logging import getLogger

from crudance import InsertMapperTemplate

from api.config.fields import FamilyFields
from api.model.engine import db_session
from api.model.schema import Family


class PutPatchFamilyController(InsertMapperTemplate):
    """Controller for PUT and PATCH family."""
    IDENTIFYING_FIELDS = [FamilyFields.id]

    def __init__(self) -> None:
        super().__init__(target=Family, session=db_session)
        self.logger = getLogger(__name__)
