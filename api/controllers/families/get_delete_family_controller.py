from logging import getLogger

from crudance import GetterDeleterTemplate

from api.config.fields import FamilyFields
from api.model.engine import db_session
from api.model.schema import Family


class GetFamilyController(GetterDeleterTemplate):
    """Controller for ``GET /family`` and ``DELETE /family``."""
    IDENTIFYING_FIELDS = [FamilyFields.id]
    TARGET_FIELDS = [FamilyFields.name]

    def __init__(self) -> None:
        super().__init__(target=Family, session=db_session)
        self.logger = getLogger(__name__)
