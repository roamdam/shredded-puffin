from http import HTTPStatus
from traceback import format_exc

from archivist import LoggerBuilder

from ..config.definitions import HTTPMessages, LAMBDA_CONTEXT_MESSAGE, GatewayInputFields
from ..config.fields import SecretFields
from ..controllers.secrets.secret_shuffle_controller import SecretShuffleController
from ..helpers.hodor import token_shield
from ..helpers.shutit import shut_session
from ..schemas.secrets import ShuffleSecretResponseSchema


@shut_session
@token_shield()
def shuffle_secrets(event: dict, context: dict):
    """Shuffle secrets lambda handler."""
    logger = LoggerBuilder().build(template="console")

    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    logger.info("Ready to shuffle secret tokens")
    logger.debug("Received secret shuffling request from %s", event[GatewayInputFields.headers])
    controller = SecretShuffleController()

    try:
        new_secrets = controller.shuffle_tokens()
    except Exception:
        logger.error("Unable to shuffle secrets : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("Secrets shuffled")
        response = ShuffleSecretResponseSchema().load({SecretFields.tokens: new_secrets})

    return response
