from http import HTTPStatus
import json
from traceback import format_exc

from archivist import LoggerBuilder
from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from ..config.definitions import (
    HTTPMessages, EndpointSensitivity, GatewayInputFields,
    LAMBDA_CONTEXT_MESSAGE, INVALID_ID_PARAMETER
)
from ..config.fields import DisciplineFields
from ..controllers.disciplines.get_delete_discipline_controller import GetDisciplineController
from ..controllers.disciplines.put_discipline_controllers import PutPatchDisciplineController
from ..controllers.disciplines.search_discipline_controller import SearchDisciplineController
from ..helpers.hodor import token_shield
from ..helpers.schema_shield import schema_shield, query_string_shield
from ..helpers.shutit import shut_session
from ..schemas.disciplines import (
    PutDisciplineSchema, PatchDisciplineSchema,
    SearchDisciplineInputSchema, SearchDisciplineOutputSchema
)


@shut_session
@schema_shield(PutDisciplineSchema)
@token_shield()
def put_discipline(event: dict, context: dict = None) -> dict:
    """Create discipline lambda handler."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PUT /discipline : %s", body)

    controller = PutPatchDisciplineController()
    try:
        discipline_id = controller.put(body)
    except Exception:
        logger.error("Unable to put discipline : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("Created discipline %s", discipline_id)
        response = {"message": "discipline created", DisciplineFields.id: discipline_id, "status": HTTPStatus.CREATED}

    return response


@shut_session
@schema_shield(PatchDisciplineSchema)
@query_string_shield(DisciplineFields.id)
@token_shield()
def patch_discipline(event: dict, context: dict = None) -> dict:
    """Update discipline lambda handler."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        discipline_id = int(event[GatewayInputFields.qparameters][DisciplineFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PATCH /discipline %s : %s", discipline_id, body)
    body[DisciplineFields.id] = discipline_id

    controller = PutPatchDisciplineController()
    try:
        controller.patch(body)
    except EntityNotFound:
        logger.error("Entity not found for PATCH /discipline %s", discipline_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for PATCH /discipline %s", discipline_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to PATCH /discipline %s : %s", discipline_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("discipline updated")
        response = {"message": "discipline updated", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@query_string_shield(DisciplineFields.id)
@token_shield()
def delete_discipline(event: dict, context: dict = None) -> dict:
    """Delete discipline lambda handler."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        discipline_id = int(event[GatewayInputFields.qparameters][DisciplineFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    logger.info("DELETE discipline %s", discipline_id)
    controller = GetDisciplineController()
    try:
        controller.delete(id=discipline_id)
    except EntityNotFound:
        logger.error("Entity not found for DELETE /discipline/%s", discipline_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for DELETE /discipline/%s", discipline_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to get discipline %s : %s", discipline_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        response = {"message": "discipline deleted", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@schema_shield(SearchDisciplineInputSchema)
@token_shield(EndpointSensitivity.everyone)
def post_search_discipline(event: dict, context: dict = None) -> dict:
    """Search disciplines lambda handler."""
    logger = LoggerBuilder().build(template="console")
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received search from POST /discipline/search : %s", body)
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    controller = SearchDisciplineController()
    try:
        found_disciplines = controller.search(**body)
    except Exception:
        logger.error("Error while searching disciplines : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.debug("Found results are %s", found_disciplines)
        response = SearchDisciplineOutputSchema().load(found_disciplines)

    return response
