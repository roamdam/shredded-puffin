from http import HTTPStatus
import json
from traceback import format_exc

from archivist import LoggerBuilder
from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from ..config.definitions import (
    HTTPMessages, EndpointSensitivity,
    LAMBDA_CONTEXT_MESSAGE, INVALID_ID_PARAMETER,
    GatewayInputFields
)
from ..config.fields import FamilyFields
from ..controllers.families.get_delete_family_controller import GetFamilyController
from ..controllers.families.put_family_controllers import PutPatchFamilyController
from ..controllers.families.search_family_controller import SearchFamilyController
from ..helpers.hodor import token_shield
from ..helpers.schema_shield import schema_shield, query_string_shield
from ..helpers.shutit import shut_session
from ..schemas.families import (
    PutFamilySchema, PatchFamilySchema,
    SearchFamilyInputSchema, SearchFamilyOutputSchema
)


@shut_session
@schema_shield(PutFamilySchema)
@token_shield()
def put_family(event: dict, context: dict):
    """Create family lambda handler."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PUT /family : %s", body)

    controller = PutPatchFamilyController()

    try:
        family_id = controller.put(body)
    except Exception:
        logger.error("Unable to put family : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("Created family %s", family_id)
        response = {"message": "family created", FamilyFields.id: family_id, "status": HTTPStatus.CREATED}

    return response


@shut_session
@schema_shield(PatchFamilySchema)
@query_string_shield(FamilyFields.id)
@token_shield()
def patch_family(event: dict, context: dict):
    """Update family lambda handler."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        family_id = int(event[GatewayInputFields.qparameters][FamilyFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PATCH /family %s : %s", family_id, body)
    body[FamilyFields.id] = family_id

    controller = PutPatchFamilyController()

    try:
        controller.patch(body)
    except EntityNotFound:
        logger.error("Entity not found for PATCH /family %s", family_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for PATCH /family %s", family_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to PATCH /family %s : %s", family_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("family updated")
        response = {"message": "family updated", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@query_string_shield(FamilyFields.id)
@token_shield()
def delete_family(event: dict, context: dict):
    """Delete family lambda handler."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        family_id = int(event[GatewayInputFields.qparameters][FamilyFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    logger.info("DELETE discipline %s", family_id)
    controller = GetFamilyController()

    try:
        controller.delete(id=family_id)
    except EntityNotFound:
        logger.error("Entity not found for DELETE /family/%s", family_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for DELETE /family/%s", family_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to delete family %s : %s", family_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        response = {"message": "family deleted", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@schema_shield(SearchFamilyInputSchema)
@token_shield(EndpointSensitivity.everyone)
def post_search_family(event: dict, context: dict):
    """Search families lambda handler."""
    logger = LoggerBuilder().build(template="console")
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received search from POST /discipline/search : %s", body)
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    controller = SearchFamilyController()

    try:
        found_families = controller.search(**body)
    except Exception:
        logger.error("Error while searching families : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.debug("Found results are %s", found_families)
        response = SearchFamilyOutputSchema().load(found_families)

    return response
