from http import HTTPStatus
import json
from traceback import format_exc

from archivist import LoggerBuilder
from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from ..config.definitions import (
    HTTPMessages, EndpointSensitivity,
    TOKEN_HEADER_KEY,
    GatewayInputFields,
    LAMBDA_CONTEXT_MESSAGE, INVALID_ID_PARAMETER
)
from ..config.fields import RecordFields
from ..controllers.records.get_delete_record_controller import GetRecordController
from ..controllers.records.put_record_controller import PutPatchRecordController
from ..controllers.records.search_record_controller import SearchRecordController
from ..helpers.hodor import token_shield, Hodor
from ..helpers.schema_shield import schema_shield, query_string_shield
from ..helpers.shutit import shut_session
from ..schemas.records import (
    PutRecordSchema, PatchRecordSchema, PostRecordsSchema,
    SearchRecordInputSchema, SearchRecordOutputSchema
)


@shut_session
@schema_shield(PutRecordSchema)
@token_shield(EndpointSensitivity.everyone)
def put_record(event: dict, context: dict):
    """Create record lambda handler."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PUT /record : %s", body)

    controller = PutPatchRecordController()

    try:
        record_id = controller.put(body)
    except Exception:
        logger.error("Unable to put record : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("Created record %s", record_id)
        response = {"message": "record created", RecordFields.id: record_id, "status": HTTPStatus.CREATED}

    return response


@shut_session
@schema_shield(PostRecordsSchema)
@token_shield(EndpointSensitivity.everyone)
def post_records(event: dict, context: dict):
    """Create or update records by batch.

    This is used to post session results or drafts, not to update.
    """
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    records = json.loads(event[GatewayInputFields.body])["records"]
    logger.info("Received batch of records from POST /record")
    logger.debug("Received records are %s", records)

    controller = PutPatchRecordController()
    errored = []
    for record in records:
        try:
            controller.put(body=record)
        except Exception:
            logger.error("Unable to put record %s, continue batch : %s", record, format_exc())
            errored.append(record)
        else:
            logger.debug("Record %s succesfully written.", record)

    return {"message": "records posted", "errors": errored, "status": HTTPStatus.OK}


@shut_session
@schema_shield(PatchRecordSchema)
@query_string_shield(RecordFields.id)
@token_shield(EndpointSensitivity.everyone)
def patch_record(event: dict, context: dict):
    """Update record."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        record_id = int(event[GatewayInputFields.qparameters][RecordFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PATCH /record %s : %s", record_id, body)
    body[RecordFields.id] = record_id

    controller = PutPatchRecordController()
    try:
        controller.patch(body)
    except EntityNotFound:
        logger.error("Entity not found for PATCH /record %s", record_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for PATCH /record %s", record_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to PATCH /record %s : %s", record_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("record updated")
        response = {"message": "record updated", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@query_string_shield(RecordFields.id)
@token_shield(EndpointSensitivity.everyone)
def delete_record(event: dict, context: dict):
    """Delete record."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        record_id = int(event[GatewayInputFields.qparameters][RecordFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    logger.info("DELETE record %s", record_id)

    controller = GetRecordController()
    try:
        controller.delete(id=record_id)
    except EntityNotFound:
        logger.error("Entity not found for DELETE /record/%s", record_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for DELETE /record/%s", record_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to delete record %s : %s", record_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        response = {"message": "record deleted", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@schema_shield(SearchRecordInputSchema)
@token_shield(EndpointSensitivity.everyone)
def post_search_record(event: dict, context: dict):
    """Search records."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received search from POST /record/search : %s", body)
    user = Hodor().find_element_from_token(token=event[GatewayInputFields.headers][TOKEN_HEADER_KEY])

    controller = SearchRecordController(user=user)
    try:
        found_records = controller.search(**body)
    except Exception:
        logger.error("Error while searching records : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.debug("Found results are %s", found_records)
        response = SearchRecordOutputSchema().load(found_records)

    return response
