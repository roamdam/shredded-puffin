from http import HTTPStatus
import json
from traceback import format_exc

from archivist import LoggerBuilder
from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from ..config.definitions import (
    HTTPMessages, EndpointSensitivity, TOKEN_HEADER_KEY,
    GatewayInputFields, LAMBDA_CONTEXT_MESSAGE
)
from ..config.fields import UserFields
from ..controllers.users.auth_controller import AuthUserController, IncorrectCredentials
from ..controllers.users.get_delete_user_controller import GetUserController
from ..controllers.users.put_user_controllers import PutPatchUserController
from ..helpers.hodor import token_shield, Hodor
from ..helpers.schema_shield import schema_shield
from ..helpers.shutit import shut_session
from ..schemas.users import PutUserSchema, PatchUserSchema, GetUserSchema, AuthUserSchema


@shut_session
@schema_shield(AuthUserSchema)
def login_user(event: dict, context: dict) -> dict:
    """Log user in lambda handler.

    If the user is found and validated, salt and token are shuffled.
    """
    logger = LoggerBuilder().build(template="console")
    body = json.loads(event[GatewayInputFields.body])
    user_email = body[UserFields.mail]
    logger.info("Received body from POST /user/login : %s", user_email)
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    controller = AuthUserController()

    try:
        user = controller.authenticate(**body)
    except IncorrectCredentials:
        logger.info("Log in refused for user %s", user_email)
        response = {"message": HTTPMessages.FORBIDDEN, "status": HTTPStatus.FORBIDDEN}
    except Exception:
        logger.error("Unable to log user in %s : %s", user_email, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("User logged in")
        response = user

    return response


@shut_session
@token_shield(EndpointSensitivity.everyone)
def logout_user(event: dict, context: dict):
    """Log authenticated user out.

    Shuffle token for authenticated user.
    """
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    user_token = event[GatewayInputFields.headers][TOKEN_HEADER_KEY]
    logger.debug("Received POST /user/logout request for %s", user_token)
    controller = AuthUserController()

    try:
        controller.logout(token=user_token)
    except Exception:
        logger.error("Unable to log out user : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("User logged out")
        response = {"message": "User logged out", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@schema_shield(PutUserSchema)
@token_shield()
def put_user(event: dict, context: dict):
    """Create user lambda handler."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PUT /user : %s", body[UserFields.mail])

    controller = PutPatchUserController()
    try:
        user_id = controller.put(body)
    except Exception:
        logger.error("Unable to put user : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("Created user %s", user_id)
        response = {"message": "User created", UserFields.id: user_id, "status": HTTPStatus.CREATED}

    return response


@shut_session
@schema_shield(PatchUserSchema)
@token_shield(EndpointSensitivity.everyone)
def patch_user(event: dict, context: dict):
    """Update user lambda handler.

    User to authenticate is taken from token.
    """
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    hodor = Hodor()
    token = event[GatewayInputFields.headers][TOKEN_HEADER_KEY]
    logger.info("PATCH /user : find user from token %s", token)

    try:
        user = hodor.find_element_from_token(token=token)
    except EntityNotFound:
        logger.error("Entity not found for PATCH /user %s", token)
        return {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple results found for PATCH /user %s", token)
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    logger.info("Received body from PATCH /user : %s", user.id)
    body = json.loads(event[GatewayInputFields.body])
    body[UserFields.id] = user.id
    controller = PutPatchUserController()

    try:
        controller.patch(body)
    except Exception:
        logger.error("Unable to PATCH /user %s : %s", user.id, format_exc())
        return {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("User updated")
        return {"message": "User updated", "status": HTTPStatus.NO_CONTENT}


@shut_session
@token_shield(EndpointSensitivity.everyone)
def get_user(event: dict, context: dict):
    """Get user from token."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    hodor = Hodor()
    token = event[GatewayInputFields.headers][TOKEN_HEADER_KEY]

    logger.info("GET /user : find user from token %s", token)
    try:
        user = hodor.find_element_from_token(token=token)
    except EntityNotFound:
        logger.error("Entity not found for GET /user/%s", token)
        return {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple results found for GET /user/%s", token)
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    logger.info("Received request GET /user for user %s", user.id)
    controller = GetUserController()

    try:
        user = controller.get(id=user.id)
    except Exception:
        logger.error("Unable to get user %s : %s", user.id, format_exc())
        return {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        return GetUserSchema().load(user)


@shut_session
@token_shield(EndpointSensitivity.everyone)
def delete_user(event: dict, context: dict):
    """Delete user.

    Only authenticated user can be deleted.
    """
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    hodor = Hodor()
    token = event[GatewayInputFields.headers][TOKEN_HEADER_KEY]

    try:
        user = hodor.find_element_from_token(token=token)
    except EntityNotFound:
        logger.error("Entity not found for DELETE /user/%s", token)
        return {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple results found for DELETE /user/%s", token)
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    logger.info("DELETE user %s", user.id)
    controller = GetUserController()

    try:
        controller.delete(id=user.id)
    except Exception:
        logger.error("Unable to get user %s : %s", user.id, format_exc())
        return {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        return {"message": "user deleted", "status": HTTPStatus.NO_CONTENT}
