from http import HTTPStatus
import json
from traceback import format_exc

from archivist import LoggerBuilder
from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from ..config.definitions import (
    HTTPMessages, EndpointSensitivity, GatewayInputFields,
    LAMBDA_CONTEXT_MESSAGE, INVALID_ID_PARAMETER
)
from ..config.fields import MovementFields
from ..controllers.movements.get_delete_movement_controller import GetMovementController
from ..controllers.movements.put_movement_controllers import PutPatchMovementController
from ..controllers.movements.search_movement_controller import SearchMovementController
from ..helpers.hodor import token_shield
from ..helpers.schema_shield import schema_shield, query_string_shield
from ..helpers.shutit import shut_session
from ..schemas.movements import (
    PutMovementSchema, PatchMovementSchema,
    SearchMovementInputSchema, SearchMovementOutputSchema
)


@shut_session
@schema_shield(PutMovementSchema)
@token_shield()
def put_movement(event: dict, context: dict):
    """Create movement."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PUT /movement : %s", body)

    controller = PutPatchMovementController()

    try:
        movement_id = controller.put(body)
    except Exception:
        logger.error("Unable to put movement : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("Created movement %s", movement_id)
        response = {"message": "movement created", MovementFields.id: movement_id, "status": HTTPStatus.CREATED}

    return response


@shut_session
@schema_shield(PatchMovementSchema)
@query_string_shield(MovementFields.id)
@token_shield()
def patch_movement(event: dict, context: dict):
    """Update movement."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        movement_id = int(event[GatewayInputFields.qparameters][MovementFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PATCH /movement %s : %s", movement_id, body)
    body[MovementFields.id] = movement_id

    controller = PutPatchMovementController()
    try:
        controller.patch(body)
    except EntityNotFound:
        logger.error("Entity not found for PATCH /movement %s", movement_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for PATCH /movement %s", movement_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to PATCH /movement %s : %s", movement_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("movement updated")
        response = {"message": "movement updated", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@query_string_shield(MovementFields.id)
@token_shield()
def delete_movement(event: dict, context: dict):
    """Delete movement."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        movement_id = int(event[GatewayInputFields.qparameters][MovementFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    logger.info("DELETE movement %s", movement_id)

    controller = GetMovementController()
    try:
        controller.delete(id=movement_id)
    except EntityNotFound:
        logger.error("Entity not found for DELETE /movement/%s", movement_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for DELETE /movement/%s", movement_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to delete movement %s : %s", movement_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        response = {"message": "movement deleted", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@token_shield(EndpointSensitivity.everyone)
@schema_shield(SearchMovementInputSchema)
def post_search_movement(event: dict, context: dict):
    """Search movements."""
    logger = LoggerBuilder().build(template="console")
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received search from POST /movement/search : %s", body)
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    controller = SearchMovementController()
    try:
        found_movements = controller.search(**body)
    except Exception:
        logger.error("Error while searching movements : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.debug("Found results are %s", found_movements)
        response = SearchMovementOutputSchema().load(found_movements)

    return response
