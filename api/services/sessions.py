from http import HTTPStatus
import json
from traceback import format_exc

from archivist import LoggerBuilder
from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from ..config.definitions import (
    HTTPMessages, EndpointSensitivity,
    TOKEN_HEADER_KEY,
    GatewayInputFields,
    LAMBDA_CONTEXT_MESSAGE, INVALID_ID_PARAMETER
)
from ..config.fields import SessionFields
from ..controllers.sessions.get_delete_session_controller import GetSessionController
from ..controllers.sessions.put_patch_session_controller import PutPatchSessionController
from ..controllers.sessions.search_session_controller import SearchSessionController
from ..helpers.hodor import Hodor, token_shield
from ..helpers.schema_shield import schema_shield, query_string_shield
from ..helpers.shutit import shut_session
from ..schemas.sessions import (
    PutSessionSchema, PatchSessionSchema, GetSessionSchema,
    SearchSessionInputSchema, SearchSessionOutputSchema
)


@shut_session
@schema_shield(PutSessionSchema)
@token_shield(EndpointSensitivity.everyone)
def put_session(event: dict, context: dict):
    """Create new session."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PUT /session : %s", body)

    user = Hodor().find_element_from_token(token=event[GatewayInputFields.headers][TOKEN_HEADER_KEY])
    controller = PutPatchSessionController(user=user)

    try:
        session_id = controller.put(body)
    except Exception:
        logger.error("Unable to put session : %s", format_exc())
        return {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}

    if session_id is None:
        logger.info("Replaced session %s", body[SessionFields.id])
        response = {"message": "Session replaced", "status": HTTPStatus.NO_CONTENT}
    else:
        logger.info("Created session %s", session_id)
        response = {"message": "Session created", SessionFields.id: session_id, "status": HTTPStatus.CREATED}

    return response


@shut_session
@schema_shield(PatchSessionSchema)
@query_string_shield(SessionFields.id)
@token_shield(EndpointSensitivity.everyone)
def patch_session(event: dict, context: dict):
    """Update session."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    try:
        session_id = int(event[GatewayInputFields.qparameters][SessionFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received body from PATCH /session : %s %s", session_id, body)
    body[SessionFields.id] = session_id

    controller = PutPatchSessionController(user=None)
    try:
        controller.patch(body)
    except EntityNotFound:
        logger.error("Entity not found for PATCH /session %s", session_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for PATCH /session %s", session_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to PATCH /session %s : %s", session_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("Session updated")
        response = {"message": "Session updated", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@query_string_shield(SessionFields.id)
@token_shield(EndpointSensitivity.everyone)
def get_session(event: dict, context: dict):
    """Get session details.

    Get session and link corresponding records."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    try:
        session_id = int(event[GatewayInputFields.qparameters][SessionFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    logger.info("Received GET /session : %s", session_id)
    controller = GetSessionController()

    try:
        session = controller.get(id=session_id)
    except EntityNotFound:
        logger.error("Entity not found for GET /session/%s", session_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for GET /session/%s", session_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to GET /session/%s : %s", session_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.info("Session retrieved")
        response = GetSessionSchema().load(session)

    return response


@shut_session
@query_string_shield(SessionFields.id)
@token_shield(EndpointSensitivity.everyone)
def delete_session(event: dict, context: dict):
    """Delete session."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)
    try:
        session_id = int(event[GatewayInputFields.qparameters][SessionFields.id])
    except ValueError:
        logger.error(INVALID_ID_PARAMETER, event[GatewayInputFields.qparameters])
        return {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

    controller = GetSessionController()
    try:
        controller.delete(id=session_id)
    except EntityNotFound:
        logger.error("Entity not found for DELETE /session/%s", session_id)
        response = {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND}
    except MultipleResultsFound:
        logger.error("Multiple resulst found for DELETE /session/%s", session_id)
        response = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
    except Exception:
        logger.error("Unable to delete session %s : %s", session_id, format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        response = {"message": "Session deleted", "status": HTTPStatus.NO_CONTENT}

    return response


@shut_session
@schema_shield(SearchSessionInputSchema)
@token_shield(EndpointSensitivity.everyone)
def post_search_session(event: dict, context: dict):
    """Search sessions."""
    logger = LoggerBuilder().build(template="console")
    logger.debug(LAMBDA_CONTEXT_MESSAGE, context)

    body = json.loads(event[GatewayInputFields.body])
    logger.info("Received search from POST /record/search : %s", body)
    user = Hodor().find_element_from_token(token=event[GatewayInputFields.headers][TOKEN_HEADER_KEY])

    controller = SearchSessionController(user=user)
    try:
        found_sessions = controller.search(**body)
    except Exception:
        logger.error("Error while searching sessions : %s", format_exc())
        response = {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR}
    else:
        logger.debug("Found results are %s", found_sessions)
        response = SearchSessionOutputSchema().load(found_sessions)

    return response
