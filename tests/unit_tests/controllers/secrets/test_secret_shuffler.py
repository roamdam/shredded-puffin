from unittest import TestCase
from unittest.mock import MagicMock, patch
from datetime import datetime

from freezegun import freeze_time

from api.config.fields import SecretFields
from api.controllers.secrets.secret_shuffle_controller import SecretShuffleController
from api.helpers.hodor import Hodor
from api.model.schema import Secret


class TestSecretShuffler(TestCase):

    def setUp(self) -> None:
        self.shuffler = SecretShuffleController()
        self.shuffler.session = MagicMock()
        self.new_token = "abcd"

    @patch.object(Hodor, "generate_token")
    @patch.object(SecretShuffleController, "_get_secrets")
    def test_shuffle_secrets(self, mo_get_secrets, mo_hodor):
        mo_hodor.return_value = self.new_token
        mo_get_secrets.return_value = [
            Secret(name="a"),
            Secret(name="b")
        ]

        actual = self.shuffler.shuffle_tokens()
        expected = [
            {
                SecretFields.name: "a",
                SecretFields.token: self.new_token
            },
            {
                SecretFields.name: "b",
                SecretFields.token: self.new_token
            }
        ]

        self.assertListEqual(expected, actual)


class TestSecretShufflerMethods(TestCase):

    def setUp(self) -> None:
        self.shuffler = SecretShuffleController()
        self.shuffler.session = MagicMock()

    def test_get_secrets(self):
        self.shuffler.session.query.return_value.all.return_value = [
            Secret(name="a"),
            Secret(name="b")
        ]
        actual = [secret.name for secret in self.shuffler._get_secrets()]
        expected = ["a", "b"]

        self.assertListEqual(expected, actual)
        self.shuffler.session.query.assert_called_once_with(Secret)

    @patch.object(Hodor, "generate_token")
    @freeze_time("2023-01-01 00:00:00")
    def test_shuffle_token(self, mo_hodor):
        secret = Secret(name="a", token="abc")

        actual = SecretShuffleController._shuffle_token(secret)
        expected = mo_hodor.return_value

        self.assertEqual(expected, actual)
        self.assertEqual(secret.token, mo_hodor.return_value)
        self.assertEqual(secret.updated_at, datetime(2023, 1, 1, 0, 0, 0))
