from unittest import TestCase

from api.controllers.disciplines.get_delete_discipline_controller import GetDisciplineController
from api.model.schema import Discipline


class TestDisciplineGetterDeleter(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = GetDisciplineController()

        self.assertEqual(controller.target, Discipline)
        controller.logger.info("I made a test")
