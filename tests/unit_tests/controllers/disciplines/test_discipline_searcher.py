from unittest import TestCase
from unittest.mock import patch

from crudance import PostSearchControllerTemplate

from api.controllers.disciplines.search_discipline_controller import SearchDisciplineController
from api.model.schema import Discipline
from api.schemas.disciplines import SearchDisciplineInputSchema


@patch.object(PostSearchControllerTemplate, "search")
class TestDisciplineSearcer(TestCase):

    def test_controller(self, _):
        """Test the __init__."""
        controller = SearchDisciplineController()

        self.assertEqual(controller.target, Discipline)
        controller.logger.info("I made a test")

    def test_search_without_filter(self, mo_parent_search):
        controller = SearchDisciplineController()
        body = {"filters": {}}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={},
            fields=SearchDisciplineInputSchema.DEFAULT_RETURN_FIELDS
        )

    def test_search_filters(self, mo_parent_search):
        controller = SearchDisciplineController()
        body = {"filters": {}, "fields": []}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={},
            fields=[]
        )
