from unittest import TestCase

from api.controllers.disciplines.put_discipline_controllers import PutPatchDisciplineController
from api.model.schema import Discipline


class TestDisciplinePutPatcher(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = PutPatchDisciplineController()

        self.assertEqual(controller.target, Discipline)
        controller.logger.info("I made a test")
