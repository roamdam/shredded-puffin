from unittest import TestCase

from api.controllers.movements.put_movement_controllers import PutPatchMovementController
from api.model.schema import Movement


class TestMovementPutPatcher(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = PutPatchMovementController()

        self.assertEqual(controller.target, Movement)
        controller.logger.info("I made a test")
