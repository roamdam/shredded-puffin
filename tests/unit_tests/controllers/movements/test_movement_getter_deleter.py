from unittest import TestCase

from api.controllers.movements.get_delete_movement_controller import GetMovementController
from api.model.schema import Movement


class TestMovementGetterDeleter(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = GetMovementController()

        self.assertEqual(controller.target, Movement)
        controller.logger.info("I made a test")
