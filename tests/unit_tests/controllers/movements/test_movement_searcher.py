from unittest import TestCase
from unittest.mock import call, MagicMock, patch

from crudance import PostSearchControllerTemplate

from api.config.fields import MovementFields
from api.controllers.movements.search_movement_controller import SearchMovementController
from api.model.schema import Movement, Family, Discipline
from api.schemas.movements import SearchMovementInputSchema


class Globals:
    class Movement:
        id = 1
        name = "Pull-up"

    class Family:
        id = 1


@patch.object(PostSearchControllerTemplate, "search")
class TestMovementsSearcher(TestCase):

    def test_controller(self, _):
        """Test the __init__."""
        controller = SearchMovementController()

        self.assertEqual(controller.target, Movement)
        controller.logger.info("I made a test")

    def test_search_without_filter(self, mo_parent_search):
        controller = SearchMovementController()
        body = {"filters": {}}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={},
            fields=SearchMovementInputSchema.DEFAULT_RETURN_FIELDS
        )

    def test_search_filters(self, mo_parent_search):
        controller = SearchMovementController()
        body = {"filters": {}, "fields": []}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={},
            fields=[]
        )


class TestControllerMethods(TestCase):

    def setUp(self) -> None:
        self.controller = SearchMovementController()

    def test_map_family(self):
        query = MagicMock()

        actual = self.controller.map_family(partial=query, filters={"family": {"id": 1}})
        expected = query.join.return_value

        self.assertEqual(actual, expected)
        query.join.assert_called_once_with(Family)

    @patch.object(SearchMovementController, "_apply_filters_clause")
    def test_map_discipline(self, mo_filters_clause):
        query = MagicMock()

        actual = self.controller.map_discipline(partial=query, filters={"discipline": {"id": 1}})
        expected = query.join.return_value

        self.assertEqual(actual, expected)
        query.join.assert_called_once()
        mo_filters_clause.assert_called_once()

    def test_serialise_movement_only(self):
        found_movement = Movement(
            id=Globals.Movement.id,
            name=Globals.Movement.name,
            family_id=Globals.Family.id
        )

        actual = self.controller._serialize(
            element=found_movement,
            selected_fields=(MovementFields.id, MovementFields.name)
        )
        expected = {
            MovementFields.id: Globals.Movement.id,
            MovementFields.name: Globals.Movement.name
        }

        self.assertDictEqual(expected, actual)

    @patch.object(PostSearchControllerTemplate, "_serialize")
    def test_serialise_full(self, mo_super_serialize):
        self.controller.session = MagicMock()
        mo_super_serialize.return_value = {}
        found_movement = Movement(
            id=Globals.Movement.id,
            name=Globals.Movement.name,
            family_id=Globals.Family.id
        )

        actual = self.controller._serialize(
            element=found_movement,
            selected_fields=(MovementFields.family, MovementFields.discipline)
        )
        expected = {
            MovementFields.discipline: mo_super_serialize.return_value,
            MovementFields.family: mo_super_serialize.return_value
        }

        self.assertDictEqual(expected, actual)
        self.controller.session.query.assert_has_calls(
            [
                call(Discipline),
                call(Family)
            ],
            any_order=True
        )
