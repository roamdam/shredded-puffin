from unittest import TestCase

from api.controllers.families.put_family_controllers import PutPatchFamilyController
from api.model.schema import Family


class TestFamilyPutPatcher(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = PutPatchFamilyController()

        self.assertEqual(controller.target, Family)
        controller.logger.info("I made a test")
