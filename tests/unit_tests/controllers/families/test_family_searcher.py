from unittest import TestCase
from unittest.mock import call, MagicMock, patch

from crudance import PostSearchControllerTemplate

from api.config.fields import FamilyFields
from api.controllers.families.search_family_controller import SearchFamilyController
from api.model.schema import Family, Discipline
from api.schemas.families import SearchFamilyInputSchema


class Globals:
    class Family:
        id = 1
        name = "Pull-ups"

    class Discipline:
        id = 1


@patch.object(PostSearchControllerTemplate, "search")
class TestFamiliesSearcher(TestCase):

    def test_controller(self, _):
        """Test the __init__."""
        controller = SearchFamilyController()

        self.assertEqual(controller.target, Family)
        controller.logger.info("I made a test")

    def test_search_without_filter(self, mo_parent_search):
        controller = SearchFamilyController()
        body = {"filters": {}}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={},
            fields=SearchFamilyInputSchema.DEFAULT_RETURN_FIELDS
        )

    def test_search_filters(self, mo_parent_search):
        controller = SearchFamilyController()
        body = {"filters": {}, "fields": []}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={},
            fields=[]
        )


class TestControllerMethods(TestCase):

    def setUp(self) -> None:
        self.controller = SearchFamilyController()

    def test_map_discipline(self):
        query = MagicMock()

        actual = self.controller.map_discipline(partial=query, filters={"discipline": {"id": 1}})
        expected = query.join.return_value

        self.assertEqual(actual, expected)
        query.join.assert_called_once()

    @patch.object(PostSearchControllerTemplate, "_serialize")
    def test_serialise_family(self, mo_super_serialize):
        self.controller.session = MagicMock()
        mo_super_serialize.return_value = {
            FamilyFields.id: Globals.Family.id,
            FamilyFields.name: Globals.Family.name
        }
        found_family = Family(
            id=Globals.Family.id,
            name=Globals.Family.name,
            discipline_id=Globals.Discipline.id
        )

        actual = self.controller._serialize(
            element=found_family,
            selected_fields=(FamilyFields.id, FamilyFields.name, FamilyFields.discipline)
        )
        expected = {
            FamilyFields.id: Globals.Family.id,
            FamilyFields.name: Globals.Family.name,
            FamilyFields.discipline: mo_super_serialize.return_value
        }

        self.assertDictEqual(expected, actual)
        self.controller.session.query.assert_has_calls(
            [
                call(Discipline)
            ],
            any_order=True
        )
