from unittest import TestCase

from api.controllers.families.get_delete_family_controller import GetFamilyController
from api.model.schema import Family


class TestFamilyGetterDeleter(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = GetFamilyController()

        self.assertEqual(controller.target, Family)
        controller.logger.info("I made a test")
