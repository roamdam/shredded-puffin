from unittest import TestCase
from unittest.mock import patch

from crudance import InsertMapperTemplate

from api.config.fields import SessionFields
from api.controllers.sessions.put_patch_session_controller import PutPatchSessionController
from api.model.schema import Session, User


class TestSessionPutter(TestCase):

    def setUp(self) -> None:
        self.user = User(id=1)

    def test_controller(self):
        """We only have to test the __init__."""
        controller = PutPatchSessionController(user=self.user)

        self.assertEqual(controller.target, Session)
        self.assertEqual(controller.user, self.user)

    @patch.object(InsertMapperTemplate, "put")
    def test_put(self, mo_put):
        controller = PutPatchSessionController(user=self.user)
        body = {SessionFields.date: "2023-01-01"}

        actual = controller.put(body=body)
        expected = mo_put.return_value

        self.assertEqual(expected, actual)
        mo_put.assert_called_once_with(body={
            SessionFields.date: "2023-01-01",
            SessionFields.user_id: 1
        })
