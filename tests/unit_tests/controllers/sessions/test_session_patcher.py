from unittest import TestCase

from api.controllers.sessions.put_patch_session_controller import PutPatchSessionController
from api.model.schema import Session


class TestSessionPatcher(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = PutPatchSessionController(user=None)

        self.assertEqual(controller.target, Session)
        controller.logger.info("I made a test")
