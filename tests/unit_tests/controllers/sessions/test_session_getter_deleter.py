from unittest import TestCase
from unittest.mock import MagicMock, patch

from api.config.fields import SessionFields, RecordFields, MovementFields
from api.controllers.records.search_record_controller import SearchRecordController
from api.controllers.sessions.get_delete_session_controller import GetSessionController
from api.model.schema import Session, Record


class Globals:
    record = Record(id=1, reps=1, movement_id=1)
    serialized_record = {
        RecordFields.id: 1,
        RecordFields.reps: 1,
        RecordFields.time: None,
        RecordFields.weight: None,
        RecordFields.movement: {
            MovementFields.id: 1,
            MovementFields.name: "Pull-up"
        }
    }


class TestSessionGetterDeleter(TestCase):

    def setUp(self) -> None:
        self.session = Session(id=1, date="2023-01-01")
        self.controller = GetSessionController()
        self.controller.session = MagicMock()

    def test_serialize_no_records(self):
        expected = {
            SessionFields.id: 1,
            SessionFields.name: None,
            SessionFields.records: []
        }

        actual = self.controller._serialize(
            element=self.session,
            selected_fields=(SessionFields.id, SessionFields.name)
        )

        self.assertDictEqual(expected, actual)

    @patch.object(SearchRecordController, "_serialize")
    def test_serialize_records(self, mo_serialize):
        self.controller.session.query.return_value.filter.return_value = [Globals.record]
        mo_serialize.return_value = Globals.serialized_record

        expected = {
            SessionFields.id: 1,
            SessionFields.name: None,
            SessionFields.records: [Globals.serialized_record]
        }
        actual = self.controller._serialize(
            element=self.session,
            selected_fields=(SessionFields.id, SessionFields.name)
        )

        self.assertDictEqual(expected, actual)
