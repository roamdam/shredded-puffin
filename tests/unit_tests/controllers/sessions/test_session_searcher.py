from unittest import TestCase
from unittest.mock import MagicMock, patch

from crudance import PostSearchControllerTemplate

from api.config.fields import SessionFields, UserFields
from api.controllers.sessions.search_session_controller import SearchSessionController
from api.model.schema import Session, User
from api.schemas.sessions import SearchSessionInputSchema


@patch.object(PostSearchControllerTemplate, "search")
class TestSessionsSearcher(TestCase):

    def setUp(self) -> None:
        self.user = User(id=1)

    def test_controller(self, _):
        """Test the __init__."""
        controller = SearchSessionController(user=self.user)

        self.assertEqual(controller.target, Session)
        controller.logger.info("I made a test")

    def test_search_without_filter(self, mo_parent_search):
        controller = SearchSessionController(user=self.user)
        body = {"filters": {}}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={
                SessionFields.user: {
                    UserFields.id: 1
                }
            },
            fields=SearchSessionInputSchema.DEFAULT_RETURN_FIELDS
        )

    def test_search_filters(self, mo_parent_search):
        controller = SearchSessionController(user=self.user)
        body = {"filters": {}, "fields": []}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={
                SessionFields.user: {
                    UserFields.id: 1
                }
            },
            fields=[]
        )


class TestControllerMethods(TestCase):

    def setUp(self) -> None:
        self.user = User(id=1)
        self.controller = SearchSessionController(user=self.user)

    def test_map_user(self):
        query = MagicMock()

        actual = self.controller.map_user(partial=query, filters={SessionFields.user: {UserFields.id: 1}})
        expected = query.join.return_value

        self.assertEqual(actual, expected)
        query.join.assert_called_once_with(User)
