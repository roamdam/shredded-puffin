from unittest import TestCase
from unittest.mock import patch

from crudance import InsertMapperTemplate

from api.config.fields import UserFields
from api.controllers.users.put_user_controllers import PutPatchUserController
from api.model.schema import User


class TestUserPutPatcher(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = PutPatchUserController()

        self.assertEqual(controller.target, User)
        controller.logger.info("I made a test")

    @patch.object(PutPatchUserController, "_expand_body_with_auth")
    @patch.object(InsertMapperTemplate, "put")
    def test_supercharge_put(self, mo_put, mo_expand_auth):
        controller = PutPatchUserController()

        actual = controller.put(body={})
        expected = mo_put.return_value

        self.assertEqual(actual, expected)
        mo_expand_auth.assert_called_once_with(body={})

    @patch.object(PutPatchUserController, "_expand_body_with_auth")
    @patch.object(InsertMapperTemplate, "patch")
    def test_supercharge_patch_password(self, mo_patch, mo_expand_auth):
        controller = PutPatchUserController()
        body = {UserFields.password: ""}

        controller.patch(body=body)

        mo_patch.assert_called_once()
        mo_expand_auth.assert_called_once_with(body)

    @patch.object(PutPatchUserController, "_expand_body_with_auth")
    @patch.object(InsertMapperTemplate, "patch")
    def test_supercharge_patch_other_field(self, mo_patch, mo_expand_auth):
        controller = PutPatchUserController()
        body = {UserFields.name: ""}

        controller.patch(body=body)

        mo_patch.assert_called_once()
        mo_expand_auth.assert_not_called()

    def test_expand_auth_infos(self):
        controller = PutPatchUserController()
        given_password = "abc"
        body = {UserFields.password: given_password}

        controller._expand_body_with_auth(body=body)

        self.assertNotEqual(body[UserFields.password], given_password)
        self.assertIn(UserFields.salt, body.keys())
        self.assertIn(UserFields.token, body.keys())
