from unittest import TestCase

from api.controllers.users.get_delete_user_controller import GetUserController
from api.model.schema import User


class TestUserGetterDeleter(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = GetUserController()

        self.assertEqual(controller.target, User)
        controller.logger.info("I made a test")
