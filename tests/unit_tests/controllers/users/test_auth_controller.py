from unittest import TestCase
from unittest.mock import patch, MagicMock

from sqlalchemy.exc import NoResultFound

from api.config.fields import UserFields
from api.controllers.users.auth_controller import AuthUserController, IncorrectCredentials
from api.controllers.users.get_delete_user_controller import GetUserController
from api.controllers.users.put_user_controllers import PutPatchUserController
from api.helpers.hodor import Hodor
from api.model.schema import User


@patch.object(PutPatchUserController, "patch")
@patch.object(GetUserController, "get")
@patch.object(AuthUserController, "_find_user_from_credentials")
class TestAuthAuthenticate(TestCase):

    def setUp(self):
        password = Hodor().encrypt_password(salt="sel", password="mot")
        self.user = User(id=1, password=password, salt="sel", token="token")
        self.controller = AuthUserController()
        self.controller.session = MagicMock()

    def test_authenticate_found(self, mo_find_user, mo_get, mo_patch):
        mo_find_user.return_value = self.user

        actual = self.controller.authenticate(email="johndoe@somewhere.com", password="mot")
        expected = mo_get.return_value

        self.assertEqual(expected, actual)
        mo_patch.assert_called_once_with({
            UserFields.id: 1,
            UserFields.password: "mot"
        })
        mo_get.assert_called_once_with(id=self.user.id)

    def test_authenticate_wrong_email(self, mo_find_user, mo_get, mo_patch):
        mo_find_user.side_effect = NoResultFound

        with self.assertRaises(IncorrectCredentials):
            self.controller.authenticate(email="", password="")

        mo_patch.assert_not_called()
        mo_get.assert_not_called()

    def test_authenticate_wrong_password(self, mo_find_user, mo_get, mo_patch):
        mo_find_user.side_effect = IncorrectCredentials

        with self.assertRaises(IncorrectCredentials):
            self.controller.authenticate(email="", password="")

        mo_patch.assert_not_called()
        mo_get.assert_not_called()

    @patch.object(Hodor, "generate_token")
    @patch.object(Hodor, "find_element_from_token")
    def test_logout(self, mo_find_user, mo_token_gen, _, mo_get, mo_patch):
        mo_find_user.return_value = self.user

        actual = self.controller.logout("token")

        self.assertEqual(actual, mo_patch.return_value)
        mo_find_user.assert_called_once_with(token="token", table=User)
        mo_patch.assert_called_once_with({
            UserFields.id: 1,
            UserFields.token: mo_token_gen.return_value
        })
        mo_get.assert_not_called()


class TestAuthPassword(TestCase):

    def setUp(self):
        password = Hodor().encrypt_password(salt="sel", password="mot")
        self.user = User(id=1, password=password, salt="sel")
        self.controller = AuthUserController()
        self.controller.session = MagicMock()

    def test_find_user_from_credentials_ok(self):
        self.controller.session.query.return_value.filter.return_value.one.return_value = self.user

        actual = self.controller._find_user_from_credentials(email="", password="mot")
        expected = self.user

        self.assertEqual(actual, expected)

    def test_find_user_wrong_password(self):
        self.controller.session.query.return_value.filter.return_value.one.return_value = self.user

        with self.assertRaises(IncorrectCredentials):
            self.controller._find_user_from_credentials(email="", password="point")
