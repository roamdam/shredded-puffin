from unittest import TestCase

from api.controllers.records.put_record_controller import PutPatchRecordController
from api.model.schema import Record


class TestRecordPutPatcher(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = PutPatchRecordController()

        self.assertEqual(controller.target, Record)
        controller.logger.info("I made a test")
