from unittest import TestCase

from api.controllers.records.get_delete_record_controller import GetRecordController
from api.model.schema import Record


class TestRecordGetterDeleter(TestCase):

    def test_controller(self):
        """We only have to test the __init__."""
        controller = GetRecordController()

        self.assertEqual(controller.target, Record)
        controller.logger.info("I made a test")
