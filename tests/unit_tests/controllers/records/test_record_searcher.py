from unittest import TestCase
from unittest.mock import call, MagicMock, patch

from crudance import PostSearchControllerTemplate

from api.config.fields import RecordFields, UserFields
from api.controllers.records.search_record_controller import SearchRecordController
from api.model.schema import Record, User, Session, Movement
from api.schemas.records import SearchRecordInputSchema


@patch.object(PostSearchControllerTemplate, "search")
class TestRecordsSearcher(TestCase):

    def setUp(self) -> None:
        self.user = User(id=1)

    def test_controller(self, _):
        """Test the __init__."""
        controller = SearchRecordController(user=self.user)

        self.assertEqual(controller.target, Record)
        controller.logger.info("I made a test")

    def test_search_without_filter(self, mo_parent_search):
        controller = SearchRecordController(user=self.user)
        body = {"filters": {}}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={
                RecordFields.user: {
                    UserFields.id: self.user.id
                }
            },
            fields=SearchRecordInputSchema.DEFAULT_RETURN_FIELDS
        )

    def test_search_filters(self, mo_parent_search):
        controller = SearchRecordController(user=self.user)
        body = {"filters": {}, "fields": []}

        actual = controller.search(**body)

        self.assertEqual(actual, mo_parent_search.return_value)
        mo_parent_search.assert_called_once_with(
            filters={
                RecordFields.user: {
                    UserFields.id: self.user.id
                }
            },
            fields=[]
        )


class TestControllerMethods(TestCase):

    def setUp(self) -> None:
        self.user = User(id=1)
        self.controller = SearchRecordController(user=self.user)
        self.record = Record(id=1, movement_id=1, session_id=1, reps=3)

    def test_map_session(self):
        query = MagicMock()

        actual = self.controller.map_session(partial=query, filters={"session": {"id": 1}})
        expected = query.join.return_value

        self.assertEqual(actual, expected)
        query.join.assert_called_once_with(Session)

    def test_map_movement(self):
        query = MagicMock()

        actual = self.controller.map_movement(partial=query, filters={"movement": {"id": 1}})
        expected = query.join.return_value

        self.assertEqual(actual, expected)
        query.join.assert_called_once_with(Movement)

    @patch.object(SearchRecordController, "_apply_filters_clause")
    def test_map_user(self, mo_filters_clause):
        query = MagicMock()

        actual = self.controller.map_user(partial=query, filters={"user": {"id": 1}})
        expected = query.join.return_value

        self.assertEqual(actual, expected)
        query.join.assert_called_once()
        mo_filters_clause.assert_called_once()

    def test_serialise_record_only(self):
        actual = self.controller._serialize(
            element=self.record,
            selected_fields=(RecordFields.movement_id, RecordFields.reps)
        )
        expected = {
            RecordFields.movement_id: 1,
            RecordFields.reps: 3
        }

        self.assertDictEqual(expected, actual)

    @patch.object(PostSearchControllerTemplate, "_serialize")
    def test_serialise_full(self, mo_super_serialize):
        self.controller.session = MagicMock()
        mo_super_serialize.return_value = {}

        actual = self.controller._serialize(
            element=self.record,
            selected_fields=(RecordFields.session, RecordFields.movement)
        )
        expected = {
            RecordFields.movement: mo_super_serialize.return_value,
            RecordFields.session: mo_super_serialize.return_value
        }

        self.assertDictEqual(expected, actual)
        self.controller.session.query.assert_has_calls(
            [
                call(Movement),
                call(Session)
            ],
            any_order=True
        )
