from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus

from sqlalchemy.exc import NoResultFound

from api.config.definitions import (
    HTTPMessages, UserPlan,
    CLIENT_SECRET_HEADER_KEY, TOKEN_HEADER_KEY,
    GatewayInputFields
)
from api.helpers.hodor import Hodor, token_shield
from api.model.schema import User


@patch.object(Hodor, "find_element_from_token")
class TestTokenShield(TestCase):
    HEADERS = {
        TOKEN_HEADER_KEY: "",
        CLIENT_SECRET_HEADER_KEY: ""
    }

    @staticmethod
    @token_shield()
    def simple_endpoint(event: dict, context: dict):
        return True

    def test_missing_token(self, _):
        expected = {"message": HTTPMessages.MISSING_TOKEN, "status": HTTPStatus.UNAUTHORIZED}
        actual = self.simple_endpoint(event={GatewayInputFields.headers: {}}, context={})

        self.assertDictEqual(expected, actual)

    def test_valid_request(self, mo_find_element_from_token):
        mo_find_element_from_token.return_value = User(plan=UserPlan.admin)

        ok_event = {
            GatewayInputFields.headers: self.HEADERS
        }
        actual = self.simple_endpoint(event=ok_event, context={})

        self.assertTrue(actual)

    def test_not_found_secret(self, mo_find_element_from_token):
        mo_find_element_from_token.side_effect = NoResultFound

        expected = {"message": HTTPMessages.FORBIDDEN, "status": HTTPStatus.FORBIDDEN}
        actual = self.simple_endpoint({GatewayInputFields.headers: self.HEADERS}, context={})

        self.assertDictEqual(expected, actual)

    def test_insufficient_privileges(self, mo_find_element_from_token):
        mo_find_element_from_token.return_value = User(plan=UserPlan.subscriber)

        expected = {"message": HTTPMessages.FORBIDDEN, "status": HTTPStatus.FORBIDDEN}
        actual = self.simple_endpoint({GatewayInputFields.headers: self.HEADERS}, context={})

        self.assertDictEqual(expected, actual)
