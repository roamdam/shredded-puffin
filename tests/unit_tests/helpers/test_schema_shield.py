from unittest import TestCase
import json
from http import HTTPStatus

from api.config.fields import UserFields
from api.config.definitions import HTTPMessages, GatewayInputFields
from api.helpers.schema_shield import schema_shield
from api.schemas.users import PatchUserSchema  # the simplest


@schema_shield(PatchUserSchema)
def decorated_lambda(event: dict, context: dict) -> True:
    return True


class TestSchemaShield(TestCase):

    def test_ok_execution(self):
        ok_event = {
            GatewayInputFields.body: json.dumps({UserFields.name: "John"})
        }

        actual = decorated_lambda(ok_event, {})

        self.assertTrue(actual)

    def test_validation_execution(self):
        wrong_event = {
            GatewayInputFields.body: json.dumps({"firstname": "John"})  # 'firstname' is not allowed in schema
        }

        expected = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}
        actual = decorated_lambda(wrong_event, {})

        self.assertDictEqual(actual, expected)
