from unittest import TestCase
from http import HTTPStatus

from api.config.definitions import GatewayInputFields, HTTPMessages
from api.config.fields import UserFields
from api.helpers.schema_shield import query_string_shield


@query_string_shield(UserFields.name)
def decorated_lambda(event: dict, context: dict) -> True:
    return True


class TestQueryStringShield(TestCase):

    def test_ok_execuction(self):
        ok_event = {
            GatewayInputFields.qparameters: {
                UserFields.name: "John"
            }
        }

        actual = decorated_lambda(ok_event, None)

        self.assertTrue(actual)

    def test_missing_qstring_parameters(self):
        missing_event = {}

        response = decorated_lambda(missing_event, None)
        expected = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

        self.assertDictEqual(expected, response)

    def test_missing_parameter(self):
        missing_parameter = {
            GatewayInputFields.qparameters: {
                UserFields.id: 1
            }
        }

        response = decorated_lambda(missing_parameter, None)
        expected = {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST}

        self.assertDictEqual(expected, response)
