from unittest import TestCase
from unittest.mock import MagicMock, patch

from api.helpers.hodor import Hodor
from api.model.schema import User


class TestUserMethods(TestCase):

    def test_find_user(self):
        hodor, user = Hodor(), User(id=1)
        hodor.session = MagicMock()
        hodor.session.query.return_value.filter.return_value.one.return_value = user

        actual = hodor.find_element_from_token(token="")
        expected = user

        self.assertEqual(actual, expected)
        hodor.session.query.assert_called_once_with(User)


class TestTokenMethods(TestCase):

    def test_generate_token(self):
        actual = Hodor.generate_token()

        self.assertIsInstance(actual, str)
        self.assertEqual(len(actual), Hodor.TOKEN_SIZE * 2)

    def test_generate_salt(self):
        actual = Hodor.generate_salt()

        self.assertIsInstance(actual, str)
        self.assertEqual(len(actual), Hodor.SALT_SIZE * 2)

    def test_password_strong(self):
        short_pwd, long_pwd = "Bonjour", "Bonjour je suis un navet pourri"

        with self.subTest("Short password is too short"):
            actual = Hodor.is_password_strong(short_pwd)
            self.assertFalse(actual)

        with self.subTest("Long password is enough"):
            actual = Hodor.is_password_strong(long_pwd)
            self.assertTrue(actual)

    @patch("api.helpers.hodor.pbkdf2_hmac")
    def test_encrypt_password(self, mo_hash):
        Hodor.encrypt_password(salt="sel", password="bonjour")

        mo_hash.assert_called_once_with(
            hash_name=Hodor.HASH_DIGEST_ALGORITHM,
            password="bonjour".encode("utf-8"),
            salt="sel".encode("utf-8"),
            iterations=Hodor.HASH_NB_ITERATIONS,
            dklen=Hodor.HASH_KEY_SIZE
        )
