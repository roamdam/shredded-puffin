from unittest import TestCase
from unittest.mock import patch

from api.helpers.shutit import shut_session


@patch("api.helpers.shutit.db_session")
class TestShutSession(TestCase):

    def test_shut_session(self, mo_session):

        @shut_session
        def handler() -> None:
            pass  # does nothing

        actual = handler()

        self.assertIsNone(actual)
        mo_session.close.assert_called_once_with()
