from unittest import TestCase
from unittest.mock import patch
from copy import deepcopy
from http import HTTPStatus
import json

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.fields import FamilyFields
from api.config.definitions import HTTPMessages, GatewayInputFields as fields
from api.services.families import put_family, patch_family, PutPatchFamilyController


class Globals:
    family_id = 1
    payload = {
        FamilyFields.name: "Gymnastics"
    }

    def __init__(self) -> None:
        self.called_payload = deepcopy(self.payload)
        self.called_payload[FamilyFields.id] = self.family_id


@patch.object(PutPatchFamilyController, "put")
class TestPutFamily(TestCase):

    @staticmethod
    def undecorated_put(*args, **kwargs):
        return put_family.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_put_family_ok(self, mo_put):
        mo_put.return_value = Globals.family_id
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        expected = {
            "message": "family created",
            FamilyFields.id: Globals.family_id,
            "status": HTTPStatus.CREATED
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, expected)
        mo_put.assert_called_once_with(Globals.payload)

    def test_put_family_error(self, mo_put):
        mo_put.side_effect = RuntimeError
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        expected = {
            "message": HTTPMessages.ERROR,
            "status": HTTPStatus.INTERNAL_SERVER_ERROR
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, expected)


@patch.object(PutPatchFamilyController, "patch")
class TestPatchFamily(TestCase):

    @staticmethod
    def undecorated_patch(*args, **kwargs):
        return patch_family.__wrapped__.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_patch_family_ok(self, mo_patch):
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                FamilyFields.id: Globals.family_id
            }
        }

        expected = {
            "message": "family updated",
            "status": HTTPStatus.NO_CONTENT
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)
        mo_patch.assert_called_once_with(Globals().called_payload)

    def test_patch_family_not_found(self, mo_patch):
        mo_patch.side_effect = EntityNotFound
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                FamilyFields.id: Globals.family_id
            }
        }

        expected = {
            "message": HTTPMessages.NOT_FOUND,
            "status": HTTPStatus.NOT_FOUND
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)

    def test_patch_family_multiple(self, mo_patch):
        mo_patch.side_effect = MultipleResultsFound
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                FamilyFields.id: Globals.family_id
            }
        }

        expected = {
            "message": HTTPMessages.BAD_REQUEST,
            "status": HTTPStatus.BAD_REQUEST
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)

    def test_patch_family_execution_error(self, mo_patch):
        mo_patch.side_effect = RuntimeError
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                FamilyFields.id: Globals.family_id
            }
        }

        expected = {
            "message": HTTPMessages.ERROR,
            "status": HTTPStatus.INTERNAL_SERVER_ERROR
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)
