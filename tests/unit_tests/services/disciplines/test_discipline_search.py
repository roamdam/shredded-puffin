from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus
import json

from api.config.definitions import HTTPMessages, GatewayInputFields as fields
from api.services.disciplines import post_search_discipline, SearchDisciplineController


@patch.object(SearchDisciplineController, "search")
class TestDisciplineSearch(TestCase):

    @staticmethod
    def undecorated_search(*args, **kwargs):
        return post_search_discipline.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_search_ok(self, mo_search):
        event = {
            fields.body: json.dumps({"filters": {}})
        }
        mo_search.return_value = {
            "data": [],
            "pagination": {
                "items": 12,
                "pages": 2
            }
        }

        actual = self.undecorated_search(event, context=None)

        self.assertDictEqual(actual, mo_search.return_value)

    def test_search_failure(self, mo_search):
        mo_search.side_effect = RuntimeError
        event = {
            fields.body: json.dumps({"filters": {}})
        }

        actual = self.undecorated_search(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
