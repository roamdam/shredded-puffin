from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.definitions import HTTPMessages, GatewayInputFields as fields
from api.config.fields import DisciplineFields
from api.services.disciplines import delete_discipline, GetDisciplineController


class Globals:
    discipline_id = 1


@patch.object(GetDisciplineController, "delete")
class TestDeleteDiscipline(TestCase):

    @staticmethod
    def undecorated_delete(*args, **kwargs):
        return delete_discipline.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_delete_discipline_ok(self, mo_delete):
        event = {
            fields.qparameters: {
                DisciplineFields.id: Globals.discipline_id
            }
        }
        actual = self.undecorated_delete(event=event, context=None)

        self.assertDictEqual(actual, {"message": "discipline deleted", "status": HTTPStatus.NO_CONTENT})
        mo_delete.assert_called_once_with(id=Globals.discipline_id)

    def test_delete_discipline_not_found(self, mo_delete):
        mo_delete.side_effect = EntityNotFound
        event = {
            fields.qparameters: {
                DisciplineFields.id: Globals.discipline_id
            }
        }
        actual = self.undecorated_delete(event=event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND})

    def test_delete_discipline_multiple(self, mo_delete):
        mo_delete.side_effect = MultipleResultsFound
        event = {
            fields.qparameters: {
                DisciplineFields.id: Globals.discipline_id
            }
        }
        actual = self.undecorated_delete(event=event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST})

    def test_delete_discipline_execution_error(self, mo_delete):
        mo_delete.side_effect = RuntimeError
        event = {
            fields.qparameters: {
                DisciplineFields.id: Globals.discipline_id
            }
        }
        actual = self.undecorated_delete(event=event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
