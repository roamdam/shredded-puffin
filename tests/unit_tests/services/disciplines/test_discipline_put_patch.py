from unittest import TestCase
from unittest.mock import patch
from copy import deepcopy
from http import HTTPStatus
import json

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.fields import DisciplineFields
from api.config.definitions import HTTPMessages, GatewayInputFields as fields
from api.services.disciplines import put_discipline, patch_discipline, PutPatchDisciplineController


class Globals:
    discipline_id = 1
    payload = {
        DisciplineFields.name: "Gymnastics"
    }

    def __init__(self) -> None:
        self.called_payload = deepcopy(self.payload)
        self.called_payload[DisciplineFields.id] = self.discipline_id


@patch.object(PutPatchDisciplineController, "put")
class TestGetDiscipline(TestCase):

    @staticmethod
    def undecorated_put(*args, **kwargs):
        return put_discipline.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_put_discipline_ok(self, mo_put):
        mo_put.return_value = Globals.discipline_id
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        expected = {
            "message": "discipline created",
            DisciplineFields.id: Globals.discipline_id,
            "status": HTTPStatus.CREATED
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, expected)
        mo_put.assert_called_once_with(Globals.payload)

    def test_put_discipline_error(self, mo_put):
        mo_put.side_effect = RuntimeError
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        expected = {
            "message": HTTPMessages.ERROR,
            "status": HTTPStatus.INTERNAL_SERVER_ERROR
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, expected)


@patch.object(PutPatchDisciplineController, "patch")
class TestDeletetdiscipline(TestCase):

    @staticmethod
    def undecorated_patch(*args, **kwargs):
        return patch_discipline.__wrapped__.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_patch_discipline_ok(self, mo_patch):
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                DisciplineFields.id: Globals.discipline_id
            }
        }

        expected = {
            "message": "discipline updated",
            "status": HTTPStatus.NO_CONTENT
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)
        mo_patch.assert_called_once_with(Globals().called_payload)

    def test_patch_discipline_not_found(self, mo_patch):
        mo_patch.side_effect = EntityNotFound
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                DisciplineFields.id: Globals.discipline_id
            }
        }

        expected = {
            "message": HTTPMessages.NOT_FOUND,
            "status": HTTPStatus.NOT_FOUND
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)

    def test_patch_discipline_multiple(self, mo_patch):
        mo_patch.side_effect = MultipleResultsFound
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                DisciplineFields.id: Globals.discipline_id
            }
        }

        expected = {
            "message": HTTPMessages.BAD_REQUEST,
            "status": HTTPStatus.BAD_REQUEST
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)

    def test_patch_discipline_execution_error(self, mo_patch):
        mo_patch.side_effect = RuntimeError
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                DisciplineFields.id: Globals.discipline_id
            }
        }

        expected = {
            "message": HTTPMessages.ERROR,
            "status": HTTPStatus.INTERNAL_SERVER_ERROR
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)
