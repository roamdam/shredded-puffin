from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus
import json

from api.config.fields import UserFields
from api.config.definitions import HTTPMessages, GatewayInputFields as fields, TOKEN_HEADER_KEY
from api.controllers.users.auth_controller import IncorrectCredentials
from api.services.users import login_user, logout_user, AuthUserController


class Globals:
    token = "token"
    payload = {
        UserFields.mail: "johndoe@somewhere.com",
        UserFields.password: "abenb"
    }


@patch.object(AuthUserController, "authenticate")
class TestAuthUser(TestCase):

    @staticmethod
    def undecorated_login(*args, **kwargs):
        return login_user.__wrapped__.__wrapped__(*args, **kwargs)

    def test_auth_user_ok(self, mo_authenticate):
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        expected = mo_authenticate.return_value
        actual = self.undecorated_login(event, context=None)

        self.assertEqual(actual, expected)
        mo_authenticate.assert_called_once_with(**Globals.payload)

    def test_auth_user_forbidden(self, mo_authenticate):
        mo_authenticate.side_effect = IncorrectCredentials
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        actual = self.undecorated_login(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.FORBIDDEN, "status": HTTPStatus.FORBIDDEN})

    def test_auth_user_error(self, mo_authenticate):
        mo_authenticate.side_effect = RuntimeError
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        actual = self.undecorated_login(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})


@patch.object(AuthUserController, "logout")
class TestLogouthUser(TestCase):

    @staticmethod
    def undecorated_logout(*args, **kwargs):
        return logout_user.__wrapped__.__wrapped__(*args, **kwargs)

    def test_logout_ok(self, mo_logout):
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }
        actual = self.undecorated_logout(event, context=None)

        self.assertDictEqual(actual, {"message": "User logged out", "status": HTTPStatus.NO_CONTENT})
        mo_logout.assert_called_once_with(token=Globals.token)

    def test_logout_error(self, mo_logout):
        mo_logout.side_effect = RuntimeError
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }
        actual = self.undecorated_logout(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
