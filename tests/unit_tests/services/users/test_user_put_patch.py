from unittest import TestCase
from unittest.mock import patch
from copy import deepcopy
from http import HTTPStatus
import json

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.fields import UserFields
from api.config.definitions import HTTPMessages, GatewayInputFields as fields, TOKEN_HEADER_KEY
from api.helpers.hodor import Hodor
from api.model.schema import User
from api.services.users import put_user, patch_user, PutPatchUserController


class Globals:
    user_id = 1
    payload = {
        UserFields.name: "John",
        UserFields.mail: "johndoe@somewhere.com",
        UserFields.password: "abenb",
        UserFields.plan: "free"
    }
    token = "token"

    def __init__(self) -> None:
        self.called_payload = deepcopy(self.payload)
        self.called_payload[UserFields.id] = self.user_id


@patch.object(PutPatchUserController, "put")
class TestPutUser(TestCase):

    @staticmethod
    def undecorated_put(*args, **kwargs):
        return put_user.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_put_user_ok(self, mo_put):
        mo_put.return_value = Globals.user_id
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, {"message": "User created", UserFields.id: Globals.user_id, "status": HTTPStatus.CREATED})  # noqa: E501
        mo_put.assert_called_once_with(Globals.payload)

    def test_put_user_error(self, mo_put):
        mo_put.side_effect = RuntimeError
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})


@patch.object(PutPatchUserController, "patch")
@patch.object(Hodor, "find_element_from_token")
class TestPatchUser(TestCase):

    @staticmethod
    def undecorated_patch(*args, **kwargs):
        return patch_user.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_patch_user_ok(self, mo_hodor, mo_patch):
        mo_hodor.return_value = User(id=Globals.user_id)
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps(Globals.payload)
        }

        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, {"message": "User updated", "status": HTTPStatus.NO_CONTENT})
        mo_patch.assert_called_once_with(Globals().called_payload)

    def test_patch_user_not_found(self, mo_hodor, mo_patch):
        mo_hodor.side_effect = EntityNotFound
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps(Globals.payload)
        }

        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND})
        mo_patch.assert_not_called()

    def test_patch_user_multiple(self, mo_hodor, mo_patch):
        mo_hodor.side_effect = MultipleResultsFound
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps(Globals.payload)
        }

        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST})
        mo_patch.assert_not_called()

    def test_patch_user_execution_error(self, mo_hodor, mo_patch):
        mo_hodor.return_value = User(id=Globals.user_id)
        mo_patch.side_effect = RuntimeError
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps(Globals.payload)
        }

        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
