from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.fields import UserFields
from api.config.definitions import HTTPMessages, GatewayInputFields as fields, TOKEN_HEADER_KEY
from api.helpers.hodor import Hodor
from api.model.schema import User
from api.services.users import delete_user, get_user, GetUserController


class Globals:
    user_id = 1
    serialised_user = {
        UserFields.name: "John",
        UserFields.mail: "johndoe@somewhere.com",
        UserFields.plan: "free"
    }
    token = "token"


@patch.object(GetUserController, "get")
@patch.object(Hodor, "find_element_from_token")
class TestGetUser(TestCase):

    @staticmethod
    def undecorated_get(*args, **kwargs):
        return get_user.__wrapped__.__wrapped__(*args, **kwargs)

    def test_get_user_ok(self, mo_hodor, mo_get):
        mo_hodor.return_value = User(id=Globals.user_id)
        mo_get.return_value = Globals.serialised_user
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }
        actual = self.undecorated_get(event, context=None)

        self.assertDictEqual(actual, Globals.serialised_user)
        mo_get.assert_called_once_with(id=Globals.user_id)

    def test_get_user_not_found(self, mo_hodor, mo_get):
        mo_hodor.side_effect = EntityNotFound
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }
        actual = self.undecorated_get(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND})
        mo_get.assert_not_called()

    def test_get_user_multiple(self, mo_hodor, mo_get):
        mo_hodor.side_effect = MultipleResultsFound
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }
        actual = self.undecorated_get(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST})
        mo_get.assert_not_called()

    def test_get_user_execution_error(self, mo_hodor, mo_get):
        mo_hodor.return_value = User(id=Globals.user_id)
        mo_get.side_effect = RuntimeError
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }
        actual = self.undecorated_get(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
        mo_get.assert_called_once_with(id=Globals.user_id)


@patch.object(GetUserController, "delete")
@patch.object(Hodor, "find_element_from_token")
class TestDeletetUser(TestCase):

    @staticmethod
    def undecorated_delete(*args, **kwargs):
        return delete_user.__wrapped__.__wrapped__(*args, **kwargs)

    def test_delete_user_ok(self, mo_hodor, mo_delete):
        mo_hodor.return_value = User(id=Globals.user_id)
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }

        actual = self.undecorated_delete(event, context=None)

        self.assertDictEqual(actual, {"message": "user deleted", "status": HTTPStatus.NO_CONTENT})
        mo_delete.assert_called_once_with(id=Globals.user_id)

    def test_delete_user_not_found(self, mo_hodor, mo_delete):
        mo_hodor.side_effect = EntityNotFound
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }

        actual = self.undecorated_delete(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND})
        mo_delete.assert_not_called()

    def test_delete_user_multiple(self, mo_hodor, mo_delete):
        mo_hodor.side_effect = MultipleResultsFound
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }

        actual = self.undecorated_delete(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST})
        mo_delete.assert_not_called()

    def test_delete_user_execution_error(self, mo_hodor, mo_delete):
        mo_hodor.return_value = User(id=Globals.user_id)
        mo_delete.side_effect = RuntimeError
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            }
        }

        actual = self.undecorated_delete(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
