from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus
import json

from api.config.definitions import HTTPMessages, GatewayInputFields as fields, TOKEN_HEADER_KEY
from api.helpers.hodor import Hodor
from api.model.schema import User
from api.services.sessions import post_search_session, SearchSessionController


class Globals:
    token = "token"


@patch.object(SearchSessionController, "search")
@patch.object(Hodor, "find_element_from_token")
class TestSessionSearch(TestCase):

    def setUp(self) -> None:
        self.user = User(id=1)

    @staticmethod
    def undecorated_search(*args, **kwargs):
        return post_search_session.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_search_ok(self, mo_find_element_from_token, mo_search):
        mo_find_element_from_token.return_value = self.user
        mo_search.return_value = {
            "data": [],
            "pagination": {
                "items": 12,
                "pages": 2
            }
        }
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps({"filters": {}})
        }

        actual = self.undecorated_search(event, context=None)

        self.assertEqual(actual, mo_search.return_value)

    def test_search_failure(self, mo_find_element_from_token, mo_search):
        mo_find_element_from_token.return_value = self.user
        mo_search.side_effect = RuntimeError
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps({"filters": {}})
        }

        actual = self.undecorated_search(event, context=None)

        self.assertEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
