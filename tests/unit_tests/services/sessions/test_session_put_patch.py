from unittest import TestCase
from unittest.mock import patch
from copy import deepcopy
from http import HTTPStatus
import json

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.fields import SessionFields
from api.config.definitions import HTTPMessages, GatewayInputFields as fields, TOKEN_HEADER_KEY
from api.helpers.hodor import Hodor
from api.model.schema import User
from api.services.sessions import put_session, patch_session, PutPatchSessionController


class Globals:
    session_id = 1
    token = "token"
    payload = {
        SessionFields.name: "Routine",
        SessionFields.comment: None,
        SessionFields.date: "2023-01-01",
        SessionFields.is_draft: True
    }

    def __init__(self) -> None:
        self.called_payload = deepcopy(self.payload)
        self.called_payload[SessionFields.id] = self.session_id


@patch.object(PutPatchSessionController, "put")
@patch.object(Hodor, "find_element_from_token")
class TestPutSession(TestCase):

    def setUp(self) -> None:
        self.user = User(id=1)
        put_payload = deepcopy(Globals.payload)
        self.payload = put_payload
        replace_payload = deepcopy(Globals.payload)
        replace_payload[SessionFields.id] = 1
        Globals.payload = replace_payload

    @staticmethod
    def undecorated_put(*args, **kwargs):
        return put_session.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_put_session_ok(self, mo_find_element_from_token, mo_put):
        mo_find_element_from_token.return_value = self.user
        mo_put.return_value = Globals.session_id

        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps(self.payload)
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, {"message": "Session created", SessionFields.id: Globals.session_id, "status": HTTPStatus.CREATED})  # noqa: E501
        mo_put.assert_called_once_with(self.payload)

    def test_put_session_error(self, mo_find_element_from_token, mo_put):
        mo_find_element_from_token.return_value = self.user
        mo_put.side_effect = RuntimeError

        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps(self.payload)
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})

    def test_replace_session_ok(self, mo_find_element_from_token, mo_put):
        mo_find_element_from_token.return_value = self.user
        mo_put.return_value = None

        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.body: json.dumps(Globals.payload)
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, {"message": "Session replaced", "status": HTTPStatus.NO_CONTENT})
        mo_put.assert_called_once_with(Globals.payload)


@patch.object(PutPatchSessionController, "patch")
class TestPatchSession(TestCase):

    @staticmethod
    def undecorated_patch(*args, **kwargs):
        return patch_session.__wrapped__.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_patch_session_ok(self, mo_patch):
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            },
            fields.body: json.dumps(Globals.payload)
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, {"message": "Session updated", "status": HTTPStatus.NO_CONTENT})
        mo_patch.assert_called_once_with(Globals().called_payload)

    def test_patch_session_not_found(self, mo_patch):
        mo_patch.side_effect = EntityNotFound
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            },
            fields.body: json.dumps(Globals.payload)
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND})

    def test_patch_session_multiple(self, mo_patch):
        mo_patch.side_effect = MultipleResultsFound
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            },
            fields.body: json.dumps(Globals.payload)
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST})

    def test_patch_session_execution_error(self, mo_patch):
        mo_patch.side_effect = RuntimeError
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: Globals.token
            },
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            },
            fields.body: json.dumps(Globals.payload)
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
