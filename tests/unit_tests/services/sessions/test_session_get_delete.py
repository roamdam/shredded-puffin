from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.definitions import HTTPMessages, GatewayInputFields as fields
from api.config.fields import SessionFields
from api.services.sessions import get_session, delete_session, GetSessionController


class Globals:
    session_id = 1
    serialized_session = {
        SessionFields.id: 1,
        SessionFields.name: "Bonjour",
        SessionFields.comment: None,
        SessionFields.date: "2021-01-01",
        SessionFields.is_draft: True,
        SessionFields.records: []
    }


@patch.object(GetSessionController, "get")
class TestGetSession(TestCase):

    @staticmethod
    def undecorated_get(*args, **kwargs):
        return get_session.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_get_session_ok(self, mo_get):
        mo_get.return_value = Globals.serialized_session
        event = {
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            }
        }

        actual = self.undecorated_get(event, context=None)

        self.assertDictEqual(actual, Globals.serialized_session)
        mo_get.assert_called_once_with(id=Globals.session_id)

    def test_get_session_not_found(self, mo_get):
        mo_get.side_effect = EntityNotFound
        event = {
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            }
        }

        actual = self.undecorated_get(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND})

    def test_get_session_multiple(self, mo_get):
        mo_get.side_effect = MultipleResultsFound
        event = {
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            }
        }

        actual = self.undecorated_get(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST})

    def test_get_session_execution_error(self, mo_get):
        mo_get.side_effect = RuntimeError
        event = {
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            }
        }

        actual = self.undecorated_get(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})


@patch.object(GetSessionController, "delete")
class TestDeletetUser(TestCase):

    @staticmethod
    def undecorated_delete(*args, **kwargs):
        return delete_session.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_delete_session_ok(self, mo_delete):
        event = {
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            }
        }

        actual = self.undecorated_delete(event, context=None)

        self.assertDictEqual(actual, {"message": "Session deleted", "status": HTTPStatus.NO_CONTENT})
        mo_delete.assert_called_once_with(id=Globals.session_id)

    def test_delete_session_not_found(self, mo_delete):
        mo_delete.side_effect = EntityNotFound
        event = {
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            }
        }

        actual = self.undecorated_delete(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND})

    def test_delete_session_multiple(self, mo_delete):
        mo_delete.side_effect = MultipleResultsFound
        event = {
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            }
        }

        actual = self.undecorated_delete(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST})

    def test_delete_session_execution_error(self, mo_delete):
        mo_delete.side_effect = RuntimeError
        event = {
            fields.qparameters: {
                SessionFields.id: Globals.session_id
            }
        }

        actual = self.undecorated_delete(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
