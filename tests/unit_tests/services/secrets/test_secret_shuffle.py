from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus

from api.config.definitions import HTTPMessages, GatewayInputFields as fields, TOKEN_HEADER_KEY
from api.config.fields import SecretFields
from api.services.secrets import shuffle_secrets, SecretShuffleController


@patch.object(SecretShuffleController, "shuffle_tokens")
class TestSecretShuffle(TestCase):

    def setUp(self) -> None:
        self.serialised_secret = {
            SecretFields.name: "a",
            SecretFields.token: "abc"
        }
        self.token = "token"

    @staticmethod
    def undecorated_shuffle(*args, **kwargs):
        return shuffle_secrets.__wrapped__.__wrapped__(*args, **kwargs)

    def test_shuffle_ok(self, mo_shuffle):
        mo_shuffle.return_value = [self.serialised_secret]
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: self.token
            }
        }

        expected = {SecretFields.tokens: [self.serialised_secret]}
        actual = self.undecorated_shuffle(event, context=None)

        self.assertDictEqual(actual, expected)
        mo_shuffle.assert_called_once_with()

    def test_shuffle_failure(self, mo_shuffle):
        mo_shuffle.side_effect = RuntimeError
        event = {
            fields.headers: {
                TOKEN_HEADER_KEY: self.token
            }
        }

        actual = self.undecorated_shuffle(event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
