from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.definitions import HTTPMessages, GatewayInputFields as fields
from api.config.fields import MovementFields
from api.services.movements import delete_movement, GetMovementController


class Globals:
    movement_id = 1


@patch.object(GetMovementController, "delete")
class TestDeleteMovement(TestCase):

    @staticmethod
    def undecorated_delete(*args, **kwargs):
        return delete_movement.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_delete_movement_ok(self, mo_delete):
        event = {
            fields.qparameters: {
                MovementFields.id: Globals.movement_id
            }
        }
        actual = self.undecorated_delete(event=event, context=None)

        self.assertDictEqual(actual, {"message": "movement deleted", "status": HTTPStatus.NO_CONTENT})
        mo_delete.assert_called_once_with(id=Globals.movement_id)

    def test_delete_movement_not_found(self, mo_delete):
        mo_delete.side_effect = EntityNotFound
        event = {
            fields.qparameters: {
                MovementFields.id: Globals.movement_id
            }
        }
        actual = self.undecorated_delete(event=event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.NOT_FOUND, "status": HTTPStatus.NOT_FOUND})

    def test_delete_movement_multiple(self, mo_delete):
        mo_delete.side_effect = MultipleResultsFound
        event = {
            fields.qparameters: {
                MovementFields.id: Globals.movement_id
            }
        }
        actual = self.undecorated_delete(event=event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.BAD_REQUEST, "status": HTTPStatus.BAD_REQUEST})

    def test_delete_movement_execution_error(self, mo_delete):
        mo_delete.side_effect = RuntimeError
        event = {
            fields.qparameters: {
                MovementFields.id: Globals.movement_id
            }
        }
        actual = self.undecorated_delete(event=event, context=None)

        self.assertDictEqual(actual, {"message": HTTPMessages.ERROR, "status": HTTPStatus.INTERNAL_SERVER_ERROR})
