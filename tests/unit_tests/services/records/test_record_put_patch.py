from unittest import TestCase
from unittest.mock import patch
from copy import deepcopy
from http import HTTPStatus
import json

from crudance import EntityNotFound
from sqlalchemy.exc import MultipleResultsFound

from api.config.fields import RecordFields
from api.config.definitions import HTTPMessages, GatewayInputFields as fields
from api.services.records import put_record, patch_record, post_records, PutPatchRecordController


class Globals:
    record_id = 1
    payload = {
        RecordFields.id: 1,
        RecordFields.session_id: 1,
        RecordFields.reps: 3,
        RecordFields.weight: 40,
        RecordFields.time: None,
        RecordFields.execution: "paused",
        RecordFields.comment: None
    }

    def __init__(self) -> None:
        self.called_payload = deepcopy(self.payload)
        self.called_payload[RecordFields.id] = self.record_id


@patch.object(PutPatchRecordController, "put")
class TestPutRecord(TestCase):

    @staticmethod
    def undecorated_put(*args, **kwargs):
        return put_record.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_put_record_ok(self, mo_put):
        mo_put.return_value = Globals.record_id
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        expected = {
            "message": "record created",
            RecordFields.id: Globals.record_id,
            "status": HTTPStatus.CREATED
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, expected)
        mo_put.assert_called_once_with(Globals.payload)

    def test_put_record_error(self, mo_put):
        mo_put.side_effect = RuntimeError
        event = {
            fields.body: json.dumps(Globals.payload)
        }

        expected = {
            "message": HTTPMessages.ERROR,
            "status": HTTPStatus.INTERNAL_SERVER_ERROR
        }
        actual = self.undecorated_put(event, context=None)

        self.assertDictEqual(actual, expected)


@patch.object(PutPatchRecordController, "put")
class TestPutBatchRecord(TestCase):

    @staticmethod
    def undecorated_post(*args, **kwargs):
        return post_records.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_put_batch_record_ok(self, mo_put):
        event = {
            fields.body: json.dumps({"records": [Globals.payload]})
        }

        actual = self.undecorated_post(event, context=None)

        self.assertDictEqual(actual, {"message": "records posted", "errors": [], "status": HTTPStatus.OK})
        mo_put.assert_called_once_with(body=Globals.payload)

    def test_put_batch_record_error(self, mo_put):
        mo_put.side_effect = RuntimeError
        event = {
            fields.body: json.dumps({"records": [Globals.payload]})
        }

        actual = self.undecorated_post(event, context=None)

        self.assertDictEqual(actual, {"message": "records posted", "errors": [Globals.payload], "status": HTTPStatus.OK})  # noqa: E501


@patch.object(PutPatchRecordController, "patch")
class TestPatchRecord(TestCase):

    @staticmethod
    def undecorated_patch(*args, **kwargs):
        return patch_record.__wrapped__.__wrapped__.__wrapped__.__wrapped__(*args, **kwargs)

    def test_patch_record_ok(self, mo_patch):
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                RecordFields.id: Globals.record_id
            }
        }

        expected = {
            "message": "record updated",
            "status": HTTPStatus.NO_CONTENT
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)
        mo_patch.assert_called_once_with(Globals().called_payload)

    def test_patch_record_not_found(self, mo_patch):
        mo_patch.side_effect = EntityNotFound
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                RecordFields.id: Globals.record_id
            }
        }

        expected = {
            "message": HTTPMessages.NOT_FOUND,
            "status": HTTPStatus.NOT_FOUND
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)

    def test_patch_record_multiple(self, mo_patch):
        mo_patch.side_effect = MultipleResultsFound
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                RecordFields.id: Globals.record_id
            }
        }

        expected = {
            "message": HTTPMessages.BAD_REQUEST,
            "status": HTTPStatus.BAD_REQUEST
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)

    def test_patch_record_execution_error(self, mo_patch):
        mo_patch.side_effect = RuntimeError
        event = {
            fields.body: json.dumps(Globals.payload),
            fields.qparameters: {
                RecordFields.id: Globals.record_id
            }
        }

        expected = {
            "message": HTTPMessages.ERROR,
            "status": HTTPStatus.INTERNAL_SERVER_ERROR
        }
        actual = self.undecorated_patch(event, context=None)

        self.assertDictEqual(actual, expected)
