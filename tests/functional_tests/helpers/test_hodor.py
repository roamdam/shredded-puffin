from unittest import TestCase

from api.helpers.hodor import Hodor


class TestHodor(TestCase):

    def test_success_comparison(self):
        db_salt, db_password = "sel", "hello"
        user_password_entry = "hello"

        db_encrypted_password = Hodor.encrypt_password(salt=db_salt, password=db_password)
        user_encrypted_password = Hodor.encrypt_password(salt=db_salt, password=user_password_entry)

        self.assertEqual(db_encrypted_password, user_encrypted_password)

    def test_failure_comparison(self):
        db_salt, db_password = "sel", "hello"
        user_password_entry = "goodbye"

        db_encrypted_password = Hodor.encrypt_password(salt=db_salt, password=db_password)
        user_encrypted_password = Hodor.encrypt_password(salt=db_salt, password=user_password_entry)

        self.assertNotEqual(db_encrypted_password, user_encrypted_password)
