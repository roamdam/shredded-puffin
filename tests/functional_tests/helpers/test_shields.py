from unittest import TestCase
from unittest.mock import patch
from http import HTTPStatus
import json

from api.config.definitions import GatewayInputFields, TOKEN_HEADER_KEY, CLIENT_SECRET_HEADER_KEY, HTTPMessages
from api.config.fields import UserFields
from api.helpers.hodor import token_shield, Hodor
from api.helpers.schema_shield import schema_shield, query_string_shield
from api.helpers.shutit import shut_session
from api.schemas.users import PatchUserSchema  # a simple one


@patch("api.helpers.shutit.db_session")
@patch.object(Hodor, "find_element_from_token")
class TestRouteProtection(TestCase):

    def test_full_protection_ok(self, _, __):

        @shut_session
        @schema_shield(PatchUserSchema)
        @query_string_shield(UserFields.id)
        @token_shield()
        def return_true(event: dict, context: dict) -> True:
            return True

        ok_event = {
            GatewayInputFields.headers: {
                TOKEN_HEADER_KEY: "",
                CLIENT_SECRET_HEADER_KEY: ""
            },
            GatewayInputFields.body: json.dumps({UserFields.name: "John"}),
            GatewayInputFields.qparameters: {UserFields.id: 1}
        }
        actual = return_true(ok_event, None)

        self.assertTrue(actual)

    def test_full_protection_token_missing(self, _, __):

        @shut_session
        @schema_shield(PatchUserSchema)
        @query_string_shield(UserFields.id)
        @token_shield()
        def return_true(event: dict, context: dict) -> True:
            return True

        ok_event = {
            GatewayInputFields.headers: {
                CLIENT_SECRET_HEADER_KEY: ""
            },
            GatewayInputFields.body: json.dumps({UserFields.name: "John"}),
            GatewayInputFields.qparameters: {UserFields.id: 1}
        }
        actual = return_true(ok_event, None)
        expected = {
            "message": HTTPMessages.MISSING_TOKEN,
            "status": HTTPStatus.UNAUTHORIZED
        }
        self.assertDictEqual(actual, expected)

    def test_full_protection_missing_qparameter(self, _, __):

        @shut_session
        @schema_shield(PatchUserSchema)
        @query_string_shield(UserFields.id)
        @token_shield()
        def return_true(event: dict, context: dict) -> True:
            return True

        ok_event = {
            GatewayInputFields.headers: {
                TOKEN_HEADER_KEY: "",
                CLIENT_SECRET_HEADER_KEY: ""
            },
            GatewayInputFields.body: json.dumps({UserFields.name: "John"}),
            GatewayInputFields.qparameters: {UserFields.mail: 1}
        }
        actual = return_true(ok_event, None)
        expected = {
            "message": HTTPMessages.BAD_REQUEST,
            "status": HTTPStatus.BAD_REQUEST
        }
        self.assertDictEqual(actual, expected)

    def test_full_protection_schema_invalidation(self, _, __):

        @shut_session
        @schema_shield(PatchUserSchema)
        @query_string_shield(UserFields.id)
        @token_shield()
        def return_true(event: dict, context: dict) -> True:
            return True

        ok_event = {
            GatewayInputFields.headers: {
                TOKEN_HEADER_KEY: "",
                CLIENT_SECRET_HEADER_KEY: ""
            },
            GatewayInputFields.body: json.dumps({"firstname": "John"}),  # not in schema
            GatewayInputFields.qparameters: {UserFields.mail: 1}
        }
        actual = return_true(ok_event, None)
        expected = {
            "message": HTTPMessages.BAD_REQUEST,
            "status": HTTPStatus.BAD_REQUEST
        }
        self.assertDictEqual(actual, expected)
