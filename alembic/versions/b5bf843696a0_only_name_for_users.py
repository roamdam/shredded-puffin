"""Only name for users

Revision ID: b5bf843696a0
Revises: 8b389a545726
Create Date: 2023-03-09 22:35:41.460697

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b5bf843696a0'
down_revision = '8b389a545726'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column('users', 'firstname', new_column_name='name')
    op.drop_column('users', 'lastname')


def downgrade() -> None:
    op.alter_column('users', 'name', new_column_name='firstname')
    op.add_column('users', sa.Column('lastname', sa.VARCHAR(), nullable=False, server_default=""))
