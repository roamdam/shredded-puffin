"""create first tables

Revision ID: e57ebf7c95b4
Revises:
Create Date: 2023-02-13 23:12:48.654760
"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "e57ebf7c95b4"
down_revision = None
branch_labels = None
depends_on = None


text_now = "now()"


def upgrade() -> None:
    op.create_table(
        "disciplines",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("created_at", sa.DateTime(), server_default=sa.text(text_now), nullable=False),
        sa.Column("updated_at", sa.DateTime(), server_default=sa.text(text_now), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("name")
    )
    op.create_table(
        "users",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("firstname", sa.String(), nullable=False),
        sa.Column("lastname", sa.String(), nullable=False),
        sa.Column("email", sa.String(), nullable=False),
        sa.Column("password", sa.String(), nullable=False),
        sa.Column("created_at", sa.DateTime(), server_default=sa.text(text_now), nullable=False),
        sa.Column("updated_at", sa.DateTime(), server_default=sa.text(text_now), nullable=False),
        sa.PrimaryKeyConstraint("id")
    )
    op.create_index(op.f("ix_users_email"), "users", ["email"], unique=True)
    op.create_table(
        "families",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("discipline_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["discipline_id"], ["disciplines.id"], onupdate="CASCADE", ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint('name', 'discipline_id')
    )
    op.create_table(
        "sessions",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("date", sa.Date(), server_default=sa.text(text_now), nullable=False),
        sa.Column("name", sa.String(), nullable=True),
        sa.Column("comment", sa.String(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["user_id"], ["users.id"], ),
        sa.PrimaryKeyConstraint("id")
    )
    op.create_table(
        "movements",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("family_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(["family_id"], ["families.id"], onupdate="CASCADE", ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint('name', 'family_id')
    )

    op.create_table(
        "records",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("session_id", sa.Integer(), nullable=False),
        sa.Column("movement_id", sa.Integer(), nullable=False),
        sa.Column("reps", sa.Integer(), nullable=False),
        sa.Column("weight", sa.Float(), nullable=True),
        sa.Column("time", sa.Integer(), nullable=True),
        sa.Column("comment", sa.String(), nullable=True),
        sa.ForeignKeyConstraint(["movement_id"], ["movements.id"], ),
        sa.ForeignKeyConstraint(["session_id"], ["sessions.id"], onupdate="CASCADE", ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("id")
    )
    op.create_index(op.f("ix_records_movement_id"), "records", ["movement_id"], unique=False)
    op.create_index(op.f("ix_records_session_id"), "records", ["session_id"], unique=False)


def downgrade() -> None:
    op.drop_index(op.f("ix_records_session_id"), table_name="records")
    op.drop_index(op.f("ix_records_movement_id"), table_name="records")
    op.drop_table("records")
    op.drop_table("movements")
    op.drop_table("sessions")
    op.drop_table("families")
    op.drop_index(op.f("ix_users_email"), table_name="users")
    op.drop_table("users")
    op.drop_table("disciplines")
