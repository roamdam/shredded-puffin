"""Add is_draft for sessions

Revision ID: b9f8b72c9186
Revises: b5bf843696a0
Create Date: 2023-03-12 22:08:38.414694

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b9f8b72c9186'
down_revision = 'b5bf843696a0'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('sessions', sa.Column('is_draft', sa.Boolean(), server_default=sa.text('false'), nullable=False))


def downgrade() -> None:
    op.drop_column('sessions', 'is_draft')
