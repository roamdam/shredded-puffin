"""Add authentication vars

Revision ID: 0ed7fd040dab
Revises: 51af60625787
Create Date: 2023-02-27 17:22:38.527970

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


# revision identifiers, used by Alembic.
revision = '0ed7fd040dab'
down_revision = '51af60625787'
branch_labels = None
depends_on = None


def upgrade() -> None:
    user_plan_enum = postgresql.ENUM(
        "free", "subscriber", "admin",
        name="user_plan_enum",
        create_type=True
    )
    user_plan_enum.create(op.get_bind())

    op.add_column('users', sa.Column('plan', user_plan_enum, server_default=sa.text("'free'::user_plan_enum"), nullable=False))
    op.add_column('users', sa.Column('salt', sa.String(), nullable=False))
    op.add_column('users', sa.Column('token', sa.String(), nullable=False))

    op.create_index(op.f('ix_users_token'), 'users', ['token'], unique=True)


def downgrade() -> None:
    op.drop_index(op.f('ix_users_token'), table_name='users')

    op.drop_column('users', 'token')
    op.drop_column('users', 'salt')
    op.drop_column('users', 'plan')

    op.execute('DROP TYPE user_plan_enum')
