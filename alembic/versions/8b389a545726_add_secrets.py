"""Add secrets

Revision ID: 8b389a545726
Revises: 53773735a9ef
Create Date: 2023-03-02 00:15:43.668754

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '8b389a545726'
down_revision = '53773735a9ef'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("""
    INSERT INTO secrets (name, token)
    VALUES
        ('local', 'local'),
        ('frontend', '')
    """)


def downgrade() -> None:
    op.execute("DELETE FROM secrets")
