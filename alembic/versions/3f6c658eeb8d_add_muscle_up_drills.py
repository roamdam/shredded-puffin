"""Add muscle up drills

Revision ID: 3f6c658eeb8d
Revises: e9fc65564196
Create Date: 2023-07-04 10:25:11.278503

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '3f6c658eeb8d'
down_revision = 'e9fc65564196'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("""
        INSERT INTO families (name, discipline_id)
        VALUES
            ('Split', (SELECT id FROM disciplines WHERE name = 'Mobility')),
            ('Pancake', (SELECT id FROM disciplines WHERE name = 'Mobility')),
            ('Backbend', (SELECT id FROM disciplines WHERE name = 'Mobility')),
            ('Fold', (SELECT id FROM disciplines WHERE name = 'Mobility'))
    """)
    op.execute("""
        INSERT INTO movements (family_id, name)
        VALUES
            ((SELECT id FROM families WHERE name = 'Muscle-up' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Gymnastics')), 'Ring pull-up'),
            ((SELECT id FROM families WHERE name = 'Muscle-up' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Gymnastics')), 'RTO hold'),
            ((SELECT id FROM families WHERE name = 'Muscle-up' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Gymnastics')), 'Hanger hold'),
            ((SELECT id FROM families WHERE name = 'Backbend' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Mobility')), 'Bridge'),
            ((SELECT id FROM families WHERE name = 'Pancake' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Mobility')), 'Horse stance'),
            ((SELECT id FROM families WHERE name = 'Pancake' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Mobility')), 'Pancake')
    """)


def downgrade() -> None:
    op.execute("""
        DELETE FROM movements
        WHERE name IN (
            'Ring pull-up',
            'RTO hold',
            'Hanger hold',
            'Bridge',
            'Horse stance',
            'Pancake'
        )
    """)
