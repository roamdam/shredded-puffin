"""Add execution for record

Revision ID: 51af60625787
Revises: d8158750ba49
Create Date: 2023-02-27 17:26:09.095562

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '51af60625787'
down_revision = 'd8158750ba49'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('records', sa.Column('execution', sa.String(), nullable=True))


def downgrade() -> None:
    op.drop_column('records', 'execution')
