"""Add secrets table

Revision ID: 53773735a9ef
Revises: 0ed7fd040dab
Create Date: 2023-03-01 22:34:16.113926

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '53773735a9ef'
down_revision = '0ed7fd040dab'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'secrets',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('token', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
        sa.Column('updated_at', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('name')
    )
    op.create_index(op.f('ix_secrets_token'), 'secrets', ['token'], unique=True)


def downgrade() -> None:
    op.drop_index(op.f('ix_secrets_token'), table_name='secrets')
    op.drop_table('secrets')
