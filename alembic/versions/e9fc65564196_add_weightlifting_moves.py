"""Add weightlifting moves

Revision ID: e9fc65564196
Revises: b9f8b72c9186
Create Date: 2023-03-23 17:09:28.058216

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = 'e9fc65564196'
down_revision = 'b9f8b72c9186'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute("""
        INSERT INTO movements (family_id, name)
        VALUES
            ((SELECT id FROM families WHERE name = 'Compound' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Weightlifting')), 'Snatch deadlift'),
            ((SELECT id FROM families WHERE name = 'Compound' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Weightlifting')), 'Clean deadlift'),
            ((SELECT id FROM families WHERE name = 'Compound' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Weightlifting')), 'Tall clean'),
            ((SELECT id FROM families WHERE name = 'Compound' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Weightlifting')), 'Tall snatch'),
            ((SELECT id FROM families WHERE name = 'Compound' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Weightlifting')), 'Hang clean'),
            ((SELECT id FROM families WHERE name = 'Compound' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Weightlifting')), 'Hang snatch'),
            ((SELECT id FROM families WHERE name = 'Compound' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Weightlifting')), 'High hang clean'),
            ((SELECT id FROM families WHERE name = 'Compound' AND discipline_id=(SELECT id FROM disciplines WHERE name = 'Weightlifting')), 'High hang snatch')
    """)


def downgrade() -> None:
    op.execute("""
        DELETE FROM movements
        WHERE name IN (
            'Snatch deadlift',
            'Clean deadlift',
            'Tall clean',
            'Tall snatch',
            'Hang clean',
            'Hang snatch',
            'High hang clean',
            'High hang snatch'
        )
    """)
