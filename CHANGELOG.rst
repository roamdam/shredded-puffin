Release notes
=============

0.3.1
-----

Add muscle-up drills movements and four mobility families

0.3.0
-----

Transform into a serverless API.

0.2.1
-----

Remove lastname from users and keep name only.

0.2.0
-----

Add authentication, API is ready.

0.1.0
-----

First released version.
